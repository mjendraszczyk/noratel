@extends('layouts.backend')

@section('content')

<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Utwórz konto!</h1>
              </div>

                  <form method="POST" action="{{ route('register') }}" class="user">
                        @csrf
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="exampleFirstName" name="name" value="{{ old('name') }}" required autofocus placeholder="Imię">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" id="exampleLastName" name="lastname" required autofocus placeholder="Nazwisko">
                  </div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control form-control-user" id="exampleInputEmail" placeholder="E-mail" name="email" value="{{ old('email') }}" required>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Hasło" name="password" required>
                  </div>
                  <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Powtórz hasło" name="password_confirmation" required>
                  </div>
                </div>
  <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('Zarejestruj') }}
                                </button>
 
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="{{ route('password.request') }}">{{ __('Zapomniałes hasła?') }}</a>
              </div>
              <div class="text-center">
                <a class="small" href="{{ route('login') }}">Masz konto? Zaloguj się!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  
@endsection
