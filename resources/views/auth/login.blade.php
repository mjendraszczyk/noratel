@extends('layouts.backend')

@section('content')

 <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">{{ __('Logowanie') }}</h1>
                  </div>
                      <form method="POST" action="{{ route('login') }}" class="user">
                        @csrf
                    <div class="form-group">
                      <input type="email" placeholder="{{ __('E-mail') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-user" name="email" value="{{ old('email') }}" required autofocus>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-user" name="password" required placeholder="{{ __('Hasło') }}">
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" type="checkbox" name="remember"   id="customCheck"{{ old('remember') ? 'checked' : '' }}>
                        <label class="custom-control-label" for="customCheck">{{ __('Zapamiętaj mnie') }}</label>
                      </div>
                    </div>

                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('Zaloguj') }}
                                </button>
                      
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="{{ route('password.request') }}">{{ __('Zapomniałes hasła?') }}</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

@endsection
