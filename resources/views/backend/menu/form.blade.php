<div class="card shadow mb-4">
    <div class="card-body">
        @if(Route::CurrentRouteName() == 'MenuEdit')
        <form method="POST" action="{{route('MenuUpdate',['id'=>$id])}}" enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            @else
            <form method="POST" action="{{route('MenuStore')}}" enctype="multipart/form-data">

                @endif
                @if ($errors->any())
                <div class="col-md-12 card border-left-danger error-border">
                    <ul class="card-body">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (!empty(Session::get('status')))
                <div class="border-left-success error-border">
                    <ul class="card-body">
                        {{Session::get('status')}}
                    </ul>
                </div>
                @endif
                @csrf


                <div class="form-group">
                    <label>Etykieta</label>
                    @if(Route::CurrentRouteName() == 'MenuEdit')
                    @foreach($menu as $p)
                    <input type="text" class="form-control form-control-Menu" value="{{$p->label}}" name="label">
                    @endforeach
                    @else
                    <input type="text" class="form-control form-control-Menu" value="{{old('label')}}" name="label">
                    @endif
                </div>


                <div class="form-group">
                    <label>Adres URL</label>
                    @if(Route::CurrentRouteName() == 'MenuEdit')
                    @foreach($menu as $p)
                    <input type="text" class="form-control form-control-Menu" value="{{$p->url}}" name="url">
                    @endforeach
                    @else
                    <input type="text" class="form-control form-control-Menu" value="{{old('url')}}" name="url">
                    @endif
                </div>


                <div class="form-group">
                    <label>Pozycja</label>
                    @if(Route::CurrentRouteName() == 'MenuEdit')
                    @foreach($menu as $p)
                    <input type="text" class="form-control form-control-Menu" value="{{$p->position}}" name="position">
                    @endforeach
                    @else
                    <input type="text" class="form-control form-control-Menu" value="{{old('position')}}"
                        name="position">
                    @endif
                </div>

                <div class="form-group">
                    <label></label>
                    <div class="form-control" style="border:0;">
                        <label>
                            @if(Route::CurrentRouteName() == 'MenuEdit')
                            @foreach($menu as $p)
                            <input type="checkbox" name="allowUrl" @if($p->allowUrl == '1') checked="checked" @endif
                            value="1">
                            @endforeach
                            @else
                            <input type="checkbox" name="allowUrl" value="1">
                            @endif
                            Aktywny
                        </label>
                    </div>
                </div>

                <div class="text-right">
                    <a href="{{Route('MenuIndex')}}" class="btn btn-light btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-arrow-left"></i>
                        </span>
                        <span class="text">Powrót</span>
                    </a>
                    <button type="submit" class="btn btn-success btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-check"></i>
                        </span>
                        <span class="text">Zapisz</span>
                    </button>
                </div>

            </form>
    </div>
</div>