@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Menu</h1>
          <p class="mb-4">
            Lista elementów
            </p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Menu</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Etykieta</th>
                      <th>Adres url</th>
                      <th>Pozycja</th>
                      <th>Opcje</th>
                    </tr>
                  </thead>

                  <tfoot>
                    <tr>
                    <th>#</th>
                 <th>Etykieta</th>
                      <th>Adres url</th>
                      <th>Pozycja</th>
                      <th>Opcje</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      @foreach($menu as $c)
                    <tr>
                      <td>{{$c->id}}</td>
                      <td>
                      {{$c->label}} 
                      </td>
                      <td>{{$c->url}}</td>
                      <td>{{$c->position}}</td>

                      <td>

<a href="{{route('MenuEdit',['id'=>$c->id])}}" class="btn btn-light btn-sm btn-icon-split">
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Edytuj</span>
                  </a>


                  <form method="POST" class="inline-form" action="{{route('MenuDestroy',['id'=>$c->id])}}">
                      @csrf
                    {{ method_field('DELETE') }}
                      <button type="submit" class="btn btn-danger btn-sm btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Usuń</span>
                </button>
          </form>
                      </td>
                    </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
               <a href="{{route('MenuCreate')}}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Utwórz</span>
            </a>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
      @endsection