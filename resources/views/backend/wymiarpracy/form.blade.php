          <div class="card shadow mb-4">
              <div class="card-body">
                  @if(Route::CurrentRouteName() == 'WymiarPracyEdit')
              <form method="POST" action="{{route('WymiarPracyUpdate',['id'=>$id])}}">
                  {{ method_field('PATCH') }}
                  @else
                  <form method="POST" action="{{route('WymiarPracyStore')}}">
                      
                  @endif
         @if ($errors->any())
    <div class="col-md-12 card border-left-danger error-border">
        <ul class="card-body">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (!empty(Session::get('status')))
    <div class="border-left-success error-border">
        <ul class="card-body">
            {{Session::get('status')}}
        </ul>
    </div>
@endif
@csrf

<div class="form-group">
    <label>Nazwa oferty</label>
    @if(Route::CurrentRouteName() == 'WymiarPracyEdit')
        @foreach($typy as $p)
                      <input type="text" class="form-control form-control-user" value="{{$p->Nazwa}}" name="Nazwa" value="" required="" autofocus="">
        @endforeach
        @else 
                      <input type="text"  class="form-control form-control-user" name="Nazwa" value="{{old('Nazwa')}}" required="" autofocus="">
        @endif
                    </div>
  
<div class="text-right">
    <a href="{{Route('WymiarPracyIndex')}}" class="btn btn-light btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Powrót</span>
        </a>
                    <button type="submit" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Zapisz</span>
        </button>
        </div>

              </form>
</div>
</div>