          <div class="card shadow mb-4">
              <div class="card-body">
                  @if(Route::CurrentRouteName() == 'AplikanciEdit')
              <form method="POST" action="{{route('AplikanciUpdate',['id'=>$id])}}" enctype="multipart/form-data">
                  {{ method_field('PATCH') }}
                  @else
                  <form method="POST" action="{{route('AplikanciStore')}}" enctype="multipart/form-data">
                      
                  @endif
     

                      @if ($errors->any())
    <div class="col-md-12 card border-left-danger error-border">
        <ul class="card-body">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (!empty(Session::get('status')))
    <div class="border-left-success error-border">
        <ul class="card-body">
            {{Session::get('status')}}
        </ul>
    </div>
@endif
@csrf

<div class="form-group">
    <label>Imię</label>
    @if(Route::CurrentRouteName() == 'AplikanciEdit')
        @foreach($Aplikant as $p)
                      <input type="text" class="form-control form-control-Newsy" value="{{$p->imie}}" name="imie"  required="" autofocus="">
        @endforeach
        @else 
                      <input type="text"  class="form-control form-control-Newsy" name="imie"  value="{{old('imie')}}" required="" autofocus="">
        @endif
                    </div>
 
              <div class="form-group">
    <label>Nazwisko</label>
    @if(Route::CurrentRouteName() == 'AplikanciEdit')
        @foreach($Aplikant as $p)
                      <input type="text"  class="form-control form-control-Newsy" name="nazwisko" value="{{$p->nazwisko}}" required="" autofocus="">
        @endforeach
        @else 
                      <input type="text"  class="form-control form-control-Newsy" name="nazwisko"   value="{{old('nazwisko')}}" required="" autofocus="">
        @endif
                    </div>
 
              <div class="form-group">
    <label>Telefon</label>
    @if(Route::CurrentRouteName() == 'AplikanciEdit')
        @foreach($Aplikant as $p)
                      <input type="text"  class="form-control form-control-Newsy" name="telefon"  value="{{$p->telefon}}"  required="" autofocus="">
        @endforeach
        @else 
                      <input type="text"  class="form-control form-control-Newsy" name="telefon"  value="{{old('telefon')}}"  required="" autofocus="">
        @endif
                    </div>

 
          <div class="form-group">
    <label>E-mail</label>
    @if(Route::CurrentRouteName() == 'AplikanciEdit')
        @foreach($Aplikant as $p)
                      <input type="text"  class="form-control form-control-Newsy" name="email"   value="{{$p->email}}" required="" autofocus="">
        @endforeach
        @else 
                      <input type="text"  class="form-control form-control-Newsy" name="email"  value="{{old('email')}}" required="" autofocus="">
        @endif
                    </div>


     
    <label>Zgoda na przetwazanie danych osobwych</label>
    <div class="form-group">
    @if(Route::CurrentRouteName() == 'AplikanciEdit')
        @foreach($Aplikant as $p)
                      <label><input type="checkbox" name="aplikant_rodo"  @if($p->zgoda_rodo == '1') checked="checked" @endif value="1" /> Zgadzam się </label>
        @endforeach
        @else 
                      <label><input type="checkbox" name="aplikant_rodo"  value="1" />  Zgadzam się</label>
        @endif
                    </div>


<div class="text-right">
    <a href="{{Route('AplikanciIndex')}}" class="btn btn-light btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Powrót</span>
        </a>
                    <button type="submit" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Zapisz</span>
        </button>
        </div>

              </form>
</div>
</div>
@if(Route::CurrentRouteName() == 'AplikanciEdit')
<h1 class="h3 mb-2 text-gray-800">Dokumenty przesłane przez kandydata</h1>
          <div class="card shadow mb-4">
              <div class="card-body">


 <div class="table-responsive">
                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Data dodania</th>
                      <th>Stanowisko</th>
                      <th>Dokument</th>
                      <th>Opcje</th>
                    </tr>
                  </thead>

                  <tfoot>
                    <tr>
                      <th>#</th>
                      <th>Data dodania</th>
                      <th>Stanowisko</th>
                      <th>Dokument</th>
                      <th>Opcje</th>
                    </tr>
                  </tfoot>
                  <tbody>
 
              @foreach($Aplikacje as $key => $aplikacja)
             <tr>
              <td>
              {{$key+1}}
        </td>
        <td>
        {{$aplikacja->created_at}}
        </td>   
        <td>
        {{App\Http\Controllers\PracaController::getPracaNazwa($aplikacja->pracaID)}}  
        </td>
        <td>
        <a href="{{asset('backend/uploads/dokumenty/'.$aplikacja->nazwaDokumentu)}}" target="_blank" class="btn btn-primary btn-sm btn-icon-split">
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Zobacz CV</span>
                  </a>
        </td>
        <td>


<a href="{{route('AplikacjaEdit',['id'=>$aplikacja->id])}}" class="btn btn-light btn-sm btn-icon-split">
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Edytuj</span>
                  </a>


                  <form method="POST" class="inline-form" action="{{route('AplikacjaDestroy',['id'=>$aplikacja->id])}}">
                      @csrf
                    {{ method_field('DELETE') }}
                      <button type="submit" class="btn btn-danger btn-sm btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Usuń</span>
                </button>
          </form>
        </td>
        </tr>
 

              @endforeach
        </tbody>
              </div>
              </div>
              @endif