          <div class="card shadow mb-4">
              <div class="card-body">
                  @if(Route::CurrentRouteName() == 'PracaEdit')
              <form method="POST" action="{{route('PracaUpdate',['id'=>$id])}}" enctype="multipart/form-data">
                  {{ method_field('PATCH') }}
                  @else
                  <form method="POST" action="{{route('PracaStore')}}" enctype="multipart/form-data">
                      
                  @endif
   
                      @if ($errors->any())
    <div class="col-md-12 card border-left-danger error-border">
        <ul class="card-body">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (!empty(Session::get('status')))
    <div class="border-left-success error-border">
        <ul class="card-body">
            {{Session::get('status')}}
        </ul>
    </div>
@endif

@csrf

<div class="form-group">
    <label>Nazwa oferty</label>
    @if(Route::CurrentRouteName() == 'PracaEdit')
        @foreach($praca as $p)
                      <input type="text" class="form-control form-control-user" value="{{$p->Nazwa}}" name="Nazwa" value="" required="" autofocus="">
        @endforeach
        @else 
                      <input type="text"  class="form-control form-control-user" value="{{old('Nazwa')}}" name="Nazwa" value="" required="" autofocus="">
        @endif
                    </div>

<label>Kategoria</label>
                    <div class="form-group">
                        <select name="Kategoria" class="form-control form-control-user">
                            @foreach($kategorie as $kategoria)
                            @if(Route::CurrentRouteName() == 'PracaEdit')
                    <option value="{{$kategoria->id}}" @if($p->Kategoria == $kategoria->id) selected="selected" @endif>{{$kategoria->Nazwa}}</option>
                            @else 
<option value="{{$kategoria->id}}">{{$kategoria->Nazwa}}</option>
                            @endif

@endforeach
                        </select>
                    </div>

<label>Opis krótki</label>
                    <div class="form-group">
                        @if(Route::CurrentRouteName() == 'PracaEdit')
        @foreach($praca as $p)
      <textarea name="Opis_krotki" class="form-control form-control-user">{{$p->Opis_krotki}}</textarea>
        @endforeach
        @else 
                      <textarea name="Opis_krotki" class="editor form-control form-control-user">{{old('Opis_krotki')}}</textarea>
        @endif
                    </div>


                    <label>Opis</label>

                                        <div class="form-group">
                        @if(Route::CurrentRouteName() == 'PracaEdit')
        @foreach($praca as $p)
      <textarea name="Opis" class="editor form-control form-control-user">{{$p->Opis}}</textarea>
        @endforeach
        @else 
                      <textarea name="Opis" class="editor form-control form-control-user">
{{old('Opis')}}
</textarea>
        @endif
                    </div>



                      <div class="form-group">
    <label>Obrazek</label>
    <div class="form-group" style="border:0;">
    <label>
    @if(Route::CurrentRouteName() == 'PracaEdit') 
      @foreach($praca as $p)
        @if($p->picture != '')
          <div class="row" style="margin:20px 0;">
            <img src="{{asset('backend/uploads/praca/'.$p->picture)}}" class="img-thumbnail" />
          </div>
        @endif
      @endforeach
      <input type="file" name="picture" />
    @else 
      <input type="file" name="picture" />
    @endif
        </label>
    </div>
</div>


<div class="form-group">
    <label>Wymiar pracy</label>
                        <select name="id_wymiarpracy" class="form-control form-control-user">
                            @foreach($WymiarPracy as $wymiar)
                            @if(Route::CurrentRouteName() == 'PracaEdit')
                    <option value="{{$wymiar->id}}" @if($p->id_wymiarpracy == $wymiar->id) selected="selected" @endif>{{$wymiar->Nazwa}}</option>
                            @else 
<option value="{{$wymiar->id}}">{{$wymiar->Nazwa}}</option>
                            @endif

@endforeach
                        </select>
                    </div>

<div class="form-group">
    <label>Typ umowy</label>
                        <select name="id_typumowy" class="form-control form-control-user">
                            @foreach($TypyUmow as $typ)
                            @if(Route::CurrentRouteName() == 'PracaEdit')
                    <option value="{{$typ->id}}" @if($p->id_typumowy == $typ->id) selected="selected" @endif>{{$typ->Nazwa}}</option>
                            @else 
<option value="{{$typ->id}}">{{$typ->Nazwa}}</option>
                            @endif

@endforeach
                        </select>
                    </div>

                    <label>Stan</label>
                    <div class="form-group">
                       <label>
                         @if(Route::CurrentRouteName() == 'PracaEdit') 
                        <input type="checkbox" name="Stan" value="1" @if($p->stan == '1') checked="checked" @endif>
                        @else
                        <input type="checkbox" name="Stan" value="1">
                        @endif
                        Aktywne
                        </label>
                    </div>

<div class="text-right">
    <a href="{{Route('PracaIndex')}}" class="btn btn-light btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Powrót</span>
        </a>
                    <button type="submit" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Zapisz</span>
        </button>
        </div>

              </form>
</div>
</div>