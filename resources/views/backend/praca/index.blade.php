@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->

          <div class="row">
        
        <div class="col-md-8">
  <h1 class="h3 mb-2 text-gray-800">Oferty pracy</h1>
          <p class="mb-4">
            Lista ofert pracy
            </p>
        </div>
        <div class="col-md-4 text-right">

          <a href="{{route('PracaCreate')}}" class="toolbar-btn btn btn-default btn-icon-split">
                    <span class="ico btn-primary flat">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Utwórz</span>
            </a>
 


        </div>
    </div>

        

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Praca</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    <th>#</th>
                      <th>Nazwa</th>
                      <th>Kategoria</th>
                      <th>Stan</th>
                      <th>Opcje</th>
                    </tr>
                  </thead>

                  <tfoot>
                    <tr>
                    <th>#</th>
                      <th>Nazwa</th>
                      <th>Kategoria</th>
                      <th>Stan</th>
                      <th>Opcje</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      @foreach($prace as $praca)
                    <tr>
                    <td>{{$praca->id}}</td>
                      <td>{{$praca->Nazwa}}</td>
                      <td> <a class="btn-sm btn-dark" href="{{route('KategoriaEdit',['id'=>$praca->Kategoria])}}">{{App\Http\Controllers\KategoriaController::getKategoriaNazwa($praca->Kategoria)}}</a></td>
                      <td>{{$praca->stan}}</td>
                      <td>

<a href="{{route('PracaEdit',['id'=>$praca->id])}}" class="btn btn-light btn-sm btn-icon-split">
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Edytuj</span>
                  </a>


                  <form method="POST" class="inline-form" action="{{route('PracaDestroy',['id'=>$praca->id])}}">
                      @csrf
                    {{ method_field('DELETE') }}
                      <button type="submit" class="btn btn-danger btn-sm btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Usuń</span>
                </button>
          </form>
                      </td>
                    </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
             
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
      @endsection