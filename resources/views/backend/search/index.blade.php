
@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Wyniki wyszukiwania</h1>
          <p class="mb-4">
            Szukana fraza: <strong>{{$query}}</strong>
            </p>
          <div class="card shadow mb-4">
          <div class="card-body">
<ol  style="padding-left: 0;">
 
<li class="card-header">{{$result[0][0]}}</li> 
<ol>

@foreach($result[1] as $key => $praca)
<li><a href="{{route('PracaEdit',['id'=>$praca->id])}}">{{$praca->Nazwa}}</a></li>
@endforeach
</ol>

<li class="card-header">{{$result[0][1]}}</li>
<ol>
@foreach($result[2] as $key => $newsy)
<li><a href="{{route('NewsyEdit',['id'=>$newsy->id])}}">{{$newsy->tytul}}</a></li>
@endforeach
</ol>

<li class="card-header">{{$result[0][2]}}</li>
<ol>
@foreach($result[3] as $key => $cms)
<li><a href="{{route('CmsEdit',['id'=>$cms->id])}}">{{$cms->tytul}}</a></li>
@endforeach
</ol>
<li class="card-header">{{$result[0][3]}}</li>
<ol>
@foreach($result[4] as $key => $aplikant)
<li><a href="{{route('AplikanciEdit',['id'=>$aplikant->id])}}">{{$aplikant->imie}} {{$aplikant->nazwisko}}</a></li>
@endforeach
</ol>
</ol>

</div>
</div>
@endsection