          <div class="card shadow mb-4">
              <div class="card-body">
                  @if(Route::CurrentRouteName() == 'CmsEdit')
              <form method="POST" action="{{route('CmsUpdate',['id'=>$id])}}" enctype="multipart/form-data">
                  {{ method_field('PATCH') }}
                  @else
                  <form method="POST" action="{{route('CmsStore')}}" enctype="multipart/form-data">
                      
                  @endif
           @if ($errors->any())
    <div class="col-md-12 card border-left-danger error-border">
        <ul class="card-body">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (!empty(Session::get('status')))
    <div class="border-left-success error-border">
        <ul class="card-body">
            {{Session::get('status')}}
        </ul>
    </div>
@endif
@csrf

<div class="form-group">
    <label>Tytuł</label>
    @if(Route::CurrentRouteName() == 'CmsEdit')
        @foreach($Cms as $p)
                      <input type="text" class="form-control form-control-Cms" value="{{$p->tytul}}" name="tytul">
        @endforeach
        @else 
                      <input type="text"  class="form-control form-control-Cms" name="tytul" value="{{old('tytul')}}">
        @endif
                    </div>


@if(Route::CurrentRouteName() == 'CmsEdit')
<div class="form-group">
<label>Link: </label>
<div class="form-control">
@foreach($Cms as $p)
<a href="{{route('cms_content',['id'=>$p->id,'title'=>str_slug($p->tytul)])}}" target="_blank">{{route('cms_content',['id'=>$p->id,'title'=>str_slug($p->tytul)])}}</a>
@endforeach
</div>
</div>
@endif

              <div class="form-group">
    <label>Krótki opis</label>
    @if(Route::CurrentRouteName() == 'CmsEdit')
        @foreach($Cms as $p)
                      <textarea class="form-control form-control-Cms" name="krotki_opis" value="" required="" autofocus="">{{$p->krotki_opis}}</textarea>
        @endforeach
        @else 
                      <textarea class="form-control form-control-Cms" name="krotki_opis" value="" required="" autofocus="">{{old('krotki_opis')}}</textarea>
        @endif
                    </div>



              <div class="form-group">
    <label>Opis</label>
    @if(Route::CurrentRouteName() == 'CmsEdit')
        @foreach($Cms as $p)
                      <textarea class="editor form-control form-control-Cms" name="opis" value="" required="" autofocus="">
                      {{$p->opis}}
                      </textarea>
        @endforeach
        @else 
                    <textarea class="editor form-control form-control-Cms" name="opis" value="" required="" autofocus="">
                    {{old('opis')}}
                    </textarea>
        @endif
                    </div>


    <div class="form-group">
    <label>Obrazek</label>
    <div class="form-group" style="border:0;">
    <label>
    @if(Route::CurrentRouteName() == 'CmsEdit') 
      @foreach($Cms as $p)
        @if($p->picture != '')
          <div class="row" style="margin:20px 0;">
            <img src="{{asset('backend/uploads/cms/'.$p->picture)}}" class="img-thumbnail" />
          </div>
        @endif
      @endforeach
      <input type="file" name="picture" />
    @else 
      <input type="file" name="picture" />
    @endif
        </label>
    </div>
</div>


              <div class="form-group">
    <label>Stan</label>
    <div class="form-control" style="border:0;">
    <label>
    @if(Route::CurrentRouteName() == 'CmsEdit')
        @foreach($Cms as $p)
        <input type="checkbox" name="Stan" @if($p->stan == '1') checked="checked" @endif value="1">
        @endforeach
        @else 
<input type="checkbox" name="Stan" value="1">
        @endif
        Aktywny
        </label>
        </div>
                    </div>
 
<div class="text-right">
    <a href="{{Route('CmsIndex')}}" class="btn btn-light btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Powrót</span>
        </a>
                    <button type="submit" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Zapisz</span>
        </button>
        </div>

              </form>
</div>
</div>