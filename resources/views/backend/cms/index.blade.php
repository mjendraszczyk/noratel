@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Strony CMS</h1>
          <p class="mb-4">
            Lista podstron
            </p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Strony CMS</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Tytuł</th>
                      <th>Krótki opis</th>
                      <th>Stan</th>
                      <th>Opcje</th>
                    </tr>
                  </thead>

                  <tfoot>
                    <tr>
                    <th>#</th>
                      <th>Tytuł</th>
                      <th>Krótki opis</th>
                      <th>Stan</th>
                      <th>Opcje</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      @foreach($cms as $c)
                    <tr>
                      <td>{{$c->id}}</td>
                      <td>{{$c->tytul}}</td>
                      <td>{{$c->krotki_opis}}</td>
                      <td>{{$c->stan}}</td>
                      <td>

<a href="{{route('CmsEdit',['id'=>$c->id])}}" class="btn btn-light btn-sm btn-icon-split">
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Edytuj</span>
                  </a>


                  <form method="POST" class="inline-form" action="{{route('CmsDestroy',['id'=>$c->id])}}">
                      @csrf
                    {{ method_field('DELETE') }}
                      <button type="submit" class="btn btn-danger btn-sm btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Usuń</span>
                </button>
          </form>
                      </td>
                    </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
               <a href="{{route('CmsCreate')}}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Utwórz</span>
            </a>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
      @endsection