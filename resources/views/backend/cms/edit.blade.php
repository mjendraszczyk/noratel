@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Strony CMS</h1>
          <p class="mb-4">
            Edycja podstron
            </p>

@include('backend.cms.form')
            @endsection