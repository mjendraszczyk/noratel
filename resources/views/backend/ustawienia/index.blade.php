@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Ustawienia</h1>
          <p class="mb-4">
            Ustawienia główne
            </p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Ustawienia</h6>
            </div>
            <div class="card-body">
                @include('backend.ustawienia.form') 
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
      @endsection