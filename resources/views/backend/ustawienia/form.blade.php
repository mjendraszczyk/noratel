 
@if (!empty(Session::get('status')))
    <div class="border-left-success error-border">
        <ul class="card-body">
            {{Session::get('status')}}
        </ul>
    </div>
@endif          
                       <form method="POST" action="{{route('SettingStore')}}">
                  {{-- {{ method_field('PATCH') }} --}}
          
@csrf          
                  
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
    <a class="nav-link active" id="glowne-tab" data-toggle="tab" href="#glowne" role="tab" aria-controls="glowne" aria-selected="false">Główne</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="firma-tab" data-toggle="tab" href="#firma" role="tab" aria-controls="firma" aria-selected="true">Dane firmy</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="social-tab" data-toggle="tab" href="#social" role="tab" aria-controls="social" aria-selected="false">Social media</a>
  </li>
    <li class="nav-item">
    <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="false">SEO</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="glowne" role="tabpanel" aria-labelledby="glowne-tab">
    

<div class="form-group">
    <label style="display:block;">Tryb konserwacji</label>
                       <label><input type="checkbox" name="maintenance"  @if(config("custom.maintenance") == '1') checked="checked" @endif value="1"> Włączony</label>
                    </div>

<div class="form-group">
    <label>Adres strony</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.url')}}" name="url" >
                    </div>

<div class="form-group">
    <label>E-mail</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.email')}}" name="email" >
                    </div>

<div class="form-group">
    <label>Analytics</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.analytics')}}" name="analytics" >
                    </div>
<div class="form-group">
    <label>Telefon do rekrutacji</label>
    <textarea class="form-control form-control-user" name="callcenter">{{config('custom.callcenter')}}</textarea>
                    </div>
</div>
  <div class="tab-pane fade" id="firma" role="tabpanel" aria-labelledby="firma-tab">

    <div class="form-group">
    <label>Nazwa firmy</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.nazwa_firmy')}}" name="nazwa_firmy" >
                    </div>

    <div class="form-group">
    <label>Adres firmy</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.adres_firmy')}}" name="adres_firmy" >
                    </div>

                        <div class="form-group">
    <label>Telefon</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.telefon_firmy')}}" name="telefon_firmy" >
                    </div>

                        <div class="form-group">
    <label>NIP</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.nip_firmy')}}" name="nip_firmy" >
                    </div>

                        <div class="form-group">
    <label>Kod</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.kod_firmy')}}" name="kod_firmy" >
                    </div>

                        <div class="form-group">
    <label>Miejscowosc</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.miejscowosc_firmy')}}" name="miejscowosc_firmy" >
                    </div>

                        <div class="form-group">
    <label>Województwo</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.wojewodztwo_firmy')}}" name="wojewodztwo_firmy" >
                    </div>
                                            <div class="form-group">
    <label>Kraj</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.kraj_firmy')}}" name="kraj_firmy" >
                    </div>

                                                                <div class="form-group">
    <label>Telefon kadry</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.tel_kadry')}}" name="tel_kadry" >
                    </div>

                                                                <div class="form-group">
    <label>E-mail kadry</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.mail_kadry')}}" name="mail_kadry" >
                    </div>

                                                                <div class="form-group">
    <label>Telefon sprzedaz</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.tel_sprzedaz')}}" name="tel_sprzedaz" >
                    </div>

                                                                <div class="form-group">
    <label>E-mail sprzedaz</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.mail_sprzedaz')}}" name="mail_sprzedaz" >
                    </div>
                                            <div class="form-group">
    <label>Informacje dodatkowe</label>
    <textarea name="info_firmy" class="form-control">{{config('custom.info_firmy')}}</textarea>
                    </div>


  </div>
  <div class="tab-pane fade" id="social" role="tabpanel" aria-labelledby="social-tab">
    
                                            <div class="form-group">
    <label>Facebook</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.facebook')}}" name="facebook" >
                    </div>

                                                                <div class="form-group">
    <label>Instagram</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.instagram')}}" name="instagram" >
                    </div>

                                                                <div class="form-group">
    <label>Google</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.google')}}" name="google" >
                    </div>

                                                                <div class="form-group">
    <label>YouTube</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.youtube')}}" name="youtube" >
                    </div>

   <div class="form-group">
    <label>LinkedIn</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.linkedin')}}" name="linkedin" >
                    </div>
  </div>
    <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
<div class="form-group">
    <label>Tytuł strony</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.title')}}" name="title" >
                    </div>

<div class="form-group">
    <label>Opis strony</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.desc')}}" name="desc" >
                    </div>

<div class="form-group">
    <label>Słowa klucze strony</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.keywords')}}" name="keywords" >
                    </div>

<div class="form-group">
    <label>Slogan</label>
                       <input type="text" class="form-control form-control-user" value="{{config('custom.slogan')}}" name="slogan" >
                    </div>

  </div>
</div>

 


<div class="text-right">
   
                    <button type="submit" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Zapisz</span>
        </button>
        </div>

              </form>
 