@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->

          <div class="row">
        
        <div class="col-md-8">
  <h1 class="h3 mb-2 text-gray-800">Aktywności</h1>
          <p class="mb-4">
            Lista ostatnich zdarzeń
            </p>
        </div>
       
    </div>
     <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Aktywności</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    <th>#</th>
                      <th>Data</th>
                      <th>Kandydat</th>
                      <th>Stanowisko</th>
                      <th>CV</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                    <th>#</th>
                      <th>Data</th>
                      <th>Kandydat</th>
                      <th>Stanowisko</th>
                      <th>CV</th>
                    </tr>
                  </tfoot>

                  <tbody>
                      @foreach($aktywnosci as $key=>$aktywnosc)
                    <tr>
                    <td>{{$key+1}}</td>
                      <td>{{$aktywnosc->created_at}}</td>
                      <td><a href="{{route('AplikanciEdit',['id'=>$aktywnosc->id])}}">{{$aktywnosc->imie}} {{$aktywnosc->nazwisko}}</a></td>
                      <td>
                      {{App\Http\Controllers\PracaController::getPracaNazwa($aktywnosc->pracaID)}}  
                      </td>
                      <td>
                        <a href="{{asset('/backend/uploads/dokumenty/'.$aktywnosc->nazwaDokumentu)}}" target="_blank" class="btn-sm btn-primary btn-icon-split">
                    <span class="icon text-white-600">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Zobacz</span>
                  </a>
 
                      </td>
                    </tr>
                        @endforeach
                   

                  </tbody>

</div>
</div>
</div>
<div class="clearfix"></div>
      @endsection