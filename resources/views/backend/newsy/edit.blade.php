@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Aktualnosci</h1>
          <p class="mb-4">
            Edycja aktualnosci
            </p>

@include('backend.newsy.form')
            @endsection