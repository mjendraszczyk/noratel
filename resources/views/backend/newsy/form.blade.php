          <div class="card shadow mb-4">
              <div class="card-body">
                  @if(Route::CurrentRouteName() == 'NewsyEdit')
              <form method="POST" action="{{route('NewsyUpdate',['id'=>$id])}}" enctype="multipart/form-data">
                  {{ method_field('PATCH') }}
                  @else
                  <form method="POST" action="{{route('NewsyStore')}}" enctype="multipart/form-data">
                      
                  @endif
    
                      @if ($errors->any())
    <div class="col-md-12 card border-left-danger error-border">
        <ul class="card-body">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (!empty(Session::get('status')))
    <div class="border-left-success error-border">
        <ul class="card-body">
            {{Session::get('status')}}
        </ul>
    </div>
@endif

@csrf

<div class="form-group">
    <label>Tytuł</label>
    @if(Route::CurrentRouteName() == 'NewsyEdit')
        @foreach($Newsy as $p)
                      <input type="text" class="form-control form-control-Newsy" value="{{$p->tytul}}" name="tytul"  required="" autofocus="">
        @endforeach
        @else 
                      <input type="text"  class="form-control form-control-Newsy" name="tytul"  value="{{old('tytul')}}"  required="" autofocus="">
        @endif
                    </div>

@if(Route::CurrentRouteName() == 'NewsyEdit')
<div class="form-group">
<label>Link: </label>
<div class="form-control">
@foreach($Newsy as $p)
<a href="{{route('aktualnosci_content',['id'=>$p->id,'title'=>str_slug($p->tytul)])}}" target="_blank">{{route('aktualnosci_content',['id'=>$p->id,'title'=>str_slug($p->tytul)])}}</a>
@endforeach
</div>
</div>
@endif           


              <div class="form-group">
    <label>Tresc</label>
    @if(Route::CurrentRouteName() == 'NewsyEdit')
        @foreach($Newsy as $p)
                      <textarea class="editor form-control form-control-Newsy" name="tresc" required="" autofocus="">
                      {{$p->tresc}}
                      </textarea>
        @endforeach
        @else 
                    <textarea class="editor form-control form-control-Newsy" name="tresc" required="" autofocus="">
                    {{old('tresc')}}
                    </textarea>
        @endif
                    </div>


              <div class="form-group">
    <label>Stan</label>
    <div class="form-control" style="border:0;">
    <label>
    @if(Route::CurrentRouteName() == 'NewsyEdit')
        @foreach($Newsy as $p)
        <input type="checkbox" name="Stan" @if($p->stan == '1') checked="checked" @endif value="1">
        @endforeach
        @else 
<input type="checkbox" name="Stan" value="1">
        @endif
        Aktywny
        </label>
        </div>
                    </div>

              <div class="form-group">
    <label>Na główna</label>
    <div class="form-control" style="border:0;">
    <label>
    @if(Route::CurrentRouteName() == 'NewsyEdit')
        @foreach($Newsy as $p)
        <input type="checkbox" name="home" @if($p->home == '1') checked="checked" @endif value="1">
        @endforeach
        @else 
<input type="checkbox" name="home" value="1">
        @endif
        Aktywny
        </label>
        </div>
                    </div>

                      <div class="form-group">
    <label>Obrazek</label>
    <div class="form-group" style="border:0;">
    <label>
    @if(Route::CurrentRouteName() == 'NewsyEdit') 
      @foreach($Newsy as $p)
        @if($p->picture != '')
          <div class="row" style="margin:20px 0;">
            <img src="{{asset('backend/uploads/newsy/'.$p->picture)}}" class="img-thumbnail" />
          </div>
        @endif
      @endforeach
      <input type="file" name="picture" />
    @else 
      <input type="file" name="picture" />
    @endif
        </label>
    </div>
</div>
 
<div class="text-right">
    <a href="{{Route('NewsyIndex')}}" class="btn btn-light btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Powrót</span>
        </a>
                    <button type="submit" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Zapisz</span>
        </button>
        </div>

              </form>
</div>
</div>