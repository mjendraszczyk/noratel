          <div class="card shadow mb-4">
              <div class="card-body">
                  @if(Route::CurrentRouteName() == 'HistoriaEdit')
              <form method="POST" action="{{route('HistoriaUpdate',['id'=>$id])}}">
                  {{ method_field('PATCH') }}
                  @else
                  <form method="POST" action="{{route('HistoriaStore')}}">
                      
                  @endif
       @if ($errors->any())
    <div class="col-md-12 card border-left-danger error-border">
        <ul class="card-body">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (!empty(Session::get('status')))
    <div class="border-left-success error-border">
        <ul class="card-body">
            {{Session::get('status')}}
        </ul>
    </div>
@endif
@csrf

<div class="form-group">
    <label>Tytuł</label>
    @if(Route::CurrentRouteName() == 'HistoriaEdit')
        @foreach($historia as $p)
                      <input type="text" class="form-control" value="{{$p->tytul}}" name="tytul">
        @endforeach
        @else 
                      <input type="text"  class="form-control" nvalue="{{old('tytul')}}" name="tytul">
        @endif
                    </div>


    <div class="form-group">
    <label>Tresc</label>
    @if(Route::CurrentRouteName() == 'HistoriaEdit')
        @foreach($historia as $p)
        <textarea class="editor form-control" name="tresc">{{$p->tresc}}</textarea>
        @endforeach
        @else 
        <textarea class="editor form-control" name="tresc">{{old('tresc')}}</textarea>
        @endif
                    </div>

 
<div class="form-group">
    <label>Pozycja</label>
    @if(Route::CurrentRouteName() == 'HistoriaEdit')
        @foreach($historia as $p)
                      <input type="text" class="form-control" value="{{$p->pozycja}}" name="pozycja">
        @endforeach
        @else 
                      <input type="text"  class="form-control" nvalue="{{old('pozycja')}}" name="pozycja">
        @endif
                    </div>
 
<div class="text-right">
    <a href="{{Route('HistoriaIndex')}}" class="btn btn-light btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Powrót</span>
        </a>
                    <button type="submit" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Zapisz</span>
        </button>
        </div>

    </form>
</div>
</div>