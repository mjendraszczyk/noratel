@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Kategorie</h1>
          <p class="mb-4">
            Lista kategorii
            </p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Kategorie</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nazwa</th>
                      <th>Opcje</th>
                    </tr>
                  </thead>

                  <tfoot>
                    <tr>
                      <th>#</th>
                    <th>Nazwa</th>

                      <th>Opcje</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      @foreach($kategorie as $kategoria)
                    <tr>
                      <td>{{$kategoria->id}}</td>
                      <td>{{$kategoria->Nazwa}}</td>
                      <td>

<a href="{{route('KategoriaEdit',['id'=>$kategoria->id])}}" class="btn btn-light btn-sm btn-icon-split">
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Edytuj</span>
                  </a>


                  <form method="POST" class="inline-form" action="{{route('KategoriaDestroy',['id'=>$kategoria->id])}}">
                      @csrf
                    {{ method_field('DELETE') }}
                      <button type="submit" class="btn btn-danger btn-sm btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Usuń</span>
                </button>
          </form>
                      </td>
                    </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
                    <a href="{{route('KategoriaCreate')}}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Utwórz</span>
            </a>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
      @endsection