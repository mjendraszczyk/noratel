          <div class="card shadow mb-4">
              <div class="card-body">
                  @if(Route::CurrentRouteName() == 'UserEdit')
              <form method="POST" action="{{route('UserUpdate',['id'=>$id])}}">
                  {{ method_field('PATCH') }}
                  @else
                  <form method="POST" action="{{route('UserStore')}}">
                      
                  @endif
 
                      @if ($errors->any())
    <div class="col-md-12 card border-left-danger error-border">
        <ul class="card-body">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (!empty(Session::get('status')))
    <div class="border-left-success error-border">
        <ul class="card-body">
            {{Session::get('status')}}
        </ul>
    </div>
@endif

@csrf

<div class="form-group">
    <label>Imię</label>
    @if(Route::CurrentRouteName() == 'UserEdit')
        @foreach($user as $p)
                      <input type="text" class="form-control form-control-user" value="{{$p->name}}" name="name" value="" required="" autofocus="">
        @endforeach
        @else 
                      <input type="text"  class="form-control form-control-user" name="name" value="{{old('name')}}" required="" autofocus="">
        @endif
                    </div>

              <div class="form-group">
    <label>Nazwisko</label>
    @if(Route::CurrentRouteName() == 'UserEdit')
        @foreach($user as $p)
                      <input type="text" class="form-control form-control-user" value="{{$p->lastname}}" name="lastname" value="" required="" autofocus="">
        @endforeach
        @else 
                      <input type="text"  class="form-control form-control-user" name="lastname" value="{{old('lastname')}}" required="" autofocus="">
        @endif
                    </div>



              <div class="form-group">
    <label>E-mail</label>
    @if(Route::CurrentRouteName() == 'UserEdit')
        @foreach($user as $p)
                      <input type="email" class="form-control form-control-user" value="{{$p->email}}" name="email" value="" required="" autofocus="">
        @endforeach
        @else 
                      <input type="email"  class="form-control form-control-user" name="email" value="{{old('email')}}" required="" autofocus="">
        @endif
                    </div>


              <div class="form-group">
    <label>Hasło</label>
    @if(Route::CurrentRouteName() == 'UserEdit')
        @foreach($user as $p)
                      <input type="password" class="form-control form-control-user" value="" name="password">
        @endforeach
        @else 
                      <input type="password"  class="form-control form-control-user" name="password">
        @endif
                    </div>


                          <div class="form-group">
    <label>Powtórz hasło</label>
    @if(Route::CurrentRouteName() == 'UserEdit')
        @foreach($user as $p)
                      <input type="password" class="form-control form-control-user" value="" name="repeat_password" >
        @endforeach
        @else 
                      <input type="password"  class="form-control form-control-user" name="repeat_password" >
        @endif
                    </div>
 

        <div class="form-group">
    <label>Rola</label>
            <select name="id_role" class="form-control form-control-user">
                            @foreach($roles as $rola)
                            @if(Route::CurrentRouteName() == 'UserEdit')
                    <option value="{{$rola->id}}" @if($p->id_role == $rola->id) selected="selected" @endif>{{$rola->Nazwa}}</option>
                            @else 
<option value="{{$rola->id}}">{{$rola->Nazwa}}</option>
                            @endif

@endforeach
                        </select>
                    </div>
 

<div class="text-right">
    <a href="{{Route('UserIndex')}}" class="btn btn-light btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Powrót</span>
        </a>
                    <button type="submit" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Zapisz</span>
        </button>
        </div>

              </form>
</div>
</div>