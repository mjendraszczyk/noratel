@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Pracownicy</h1>
          <p class="mb-4">
            Edycja pracowników
            </p>

@include('backend.user.form')
            @endsection