          <div class="card shadow mb-4">
              <div class="card-body">
                  @if(Route::CurrentRouteName() == 'AplikacjaEdit')
              <form method="POST" action="{{route('AplikacjaUpdate',['id'=>$id])}}" enctype="multipart/form-data">
                  {{ method_field('PATCH') }}
                  @else
                  <form method="POST" action="{{route('AplikacjaStore')}}" enctype="multipart/form-data">
                      
                  @endif
                  
                      @if ($errors->any())
    <div class="col-md-12 card border-left-danger error-border">
        <ul class="card-body">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (!empty(Session::get('status')))
    <div class="border-left-success error-border">
        <ul class="card-body">
            {{Session::get('status')}}
        </ul>
    </div>
@endif

@csrf

                   <div class="form-group">
    <label>Dokument</label>
    <div class="form-group" style="border:0;">
    <label>
    @if(Route::CurrentRouteName() == 'AplikacjaEdit') 
      @foreach($Aplikanci as $p)
        @if($p->nazwaDokumentu != '')
          <div class="row" style="margin:20px 0;">
          <a href="{{asset('backend/uploads/dokumenty/'.$p->nazwaDokumentu)}}" class="btn btn-default">Zobacz dokument</a>
          </div>
        @endif
      @endforeach
      <input type="file" name="nazwaDokumentu" />
    @else 
      <input type="file" name="nazwaDokumentu" />
    @endif
        </label>
    </div>
</div>


 
              <div class="form-group">
    <label>Kandydat</label>
 
     <select name="aplikantID" class="form-control form-control-user">
                            @foreach($Aplikanci as $aplikant)
                            @if(Route::CurrentRouteName() == 'AplikacjaEdit')
                    <option value="{{$aplikant->id}}" @if($p->aplikantID == $aplikant->id) selected="selected" @endif>#{{$aplikant->id}} - {{$aplikant->imie}} {{$aplikant->nazwisko}}</option>
                            @else 
<option value="{{$aplikant->id}}">#{{$aplikant->id}} - {{$aplikant->imie}} {{$aplikant->nazwisko}}</option>
                            @endif

@endforeach
                        </select>

                    </div>
 
       
        <div class="form-group">
    <label>Praca</label>
 
     <select name="pracaID" class="form-control form-control-user">
                            @foreach($Prace as $praca)
                            @if(Route::CurrentRouteName() == 'AplikacjaEdit')
                    <option value="{{$praca->id}}" @if($p->pracaID == $praca->id) selected="selected" @endif>{{$praca->Nazwa}}</option>
                            @else 
<option value="{{$praca->id}}">{{$praca->Nazwa}}</option>
                            @endif

@endforeach
                        </select>

                    </div>

 

<div class="text-right">
    <a href="{{Route('AplikacjaIndex')}}" class="btn btn-light btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-arrow-left"></i>
                    </span>
                    <span class="text">Powrót</span>
        </a>
                    <button type="submit" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Zapisz</span>
        </button>
        </div>

              </form>
</div>
</div>