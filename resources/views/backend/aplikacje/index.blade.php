@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Dokumenty</h1>
          <p class="mb-4">
            Lista dokumentów
            </p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Kandydaci</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Nazwa pliku</th>
                      <th>Kandydat</th>
                      <th>Data dodania</th>
                      <th>Opcje</th>
                    </tr>
                  </thead>

                  <tfoot>
                    <tr>
                     <th>#</th>
                      <th>Nazwa pliku</th>
                      <th>Kandydat</th>
                      <th>Data dodania</th>
                      <th>Opcje</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      @foreach($Aplikacja as $c)
                    <tr>
                      <td>{{$c->id}}</td>
                      <td>
                          {{$c->nazwaDokumentu}}
                      </td>
                      <td>
                      @if((App\Http\Controllers\AplikantController::getKandydatName($c->aplikantID)) != '<Nie podano>')<a href="{{route('AplikanciEdit',['id'=>$c->aplikantID])}}">@endif 
                      {{App\Http\Controllers\AplikantController::getKandydatName($c->aplikantID)}}
                      @if((App\Http\Controllers\AplikantController::getKandydatName($c->aplikantID)) != '<Nie podano>')
                      </a>
                      @endif
                      </td>
                      <td>{{$c->created_at}}</td>
                      <td>
<a href="{{asset('backend/uploads/dokumenty/'.$c->nazwaDokumentu)}}" target="_blank" class="btn-sm btn-primary btn-icon-split">
                    <span class="icon text-white-600">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Zobacz</span>
                  </a>

<a href="{{route('AplikacjaEdit',['id'=>$c->id])}}" class="btn btn-light btn-sm btn-icon-split">
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Edytuj</span>
                  </a>


                  <form method="POST" class="inline-form" action="{{route('AplikacjaDestroy',['id'=>$c->id])}}">
                      @csrf
                    {{ method_field('DELETE') }}
                      <button type="submit" class="btn btn-danger btn-sm btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Usuń</span>
                </button>
          </form>
                      </td>
                    </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
               <a href="{{route('AplikacjaCreate')}}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
                    <span class="text">Utwórz</span>
            </a>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
      @endsection