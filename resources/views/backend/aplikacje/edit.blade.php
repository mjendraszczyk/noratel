@extends('layouts.backend')

@section('content')
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Dokumenty</h1>
          <p class="mb-4">
            Edycja dokumentów
            </p>

@include('backend.aplikacje.form')
            @endsection