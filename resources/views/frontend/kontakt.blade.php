@extends('layouts.frontend') 
@section('content')
<div class="container">
  <main class="kontakt">
    <h1 class="card-title">Kontakt</h1>

    <div class="row" style="width:100%;">

    @if ($errors->any())
    <div class="col-md-12 card border-left-danger error-border">
        <ul class="card-body">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (!empty(Session::get('status')))
    <div class="card col-md-12 border-left-success error-border">
        <ul class="card-body">
            {{Session::get('status')}}
        </ul>
    </div>
@endif

<div class="col-md-4 card flat">
<div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Dane firmy</h6>
          </div>
          <div class="card-body">
<h6>Adres</h6>
{{config('custom.nazwa_firmy')}}<br/>
{{config('custom.adres_firmy')}}<br/>
{{config('custom.kod_firmy')}} {{config('custom.miejscowosc_firmy')}}<br/>
{{config('custom.kraj_firmy')}}<br/>
<br/>
<h6>Sekretariat:</h6>
<i class="fa fa-phone"></i> {{config('custom.telefon_firmy')}}
<br/>
<i class="fa fa-envelope-o"></i> {{config('custom.email')}}
<h6 style="margin: 10px 0;">Kadry / Rekrutacja</h6>
<i class="fa fa-phone"></i> {{config('custom.tel_kadry')}} 
<br/>
<i class="fa fa-envelope-o"></i> {{config('custom.mail_kadry')}} 
<h6 style="margin: 10px 0;">Sprzedaż:</h6>
<i class="fa fa-phone"></i>{{config('custom.tel_sprzedaz')}}  
<br/>
<i class="fa fa-envelope-o"></i> {{config('custom.mail_sprzedaz')}} 
          </div>
</div>

<div class="col-md-8">
  <form method="POST" action="{{route('sendMail')}}">
@csrf
    <div class="row">
      <div class="col-md-6"> 
<div class="form-group">
<label>Odbiorca</label>
<select class="form-control form-control-user" name="kontakt">
@foreach (Config::get('kontakty') as $kontakty)
<option value="{{$kontakty}}">{{$kontakty}}</option>
@endforeach
</select>
</div>
        <div class="form-group">
<label>Imię i nazwisko</label>
<input type="text"  class="form-control form-control-user" name="imie" value="{{old('imie')}}">
</div>
<div class="form-group">
<label>Temat wiadomości</label>
<input type="text"  class="form-control form-control-user" name="temat" value="{{old('temat')}}">
</div>

<div class="form-group">
<label>Twój e-mail</label>
<input type="email"  class="form-control form-control-user" name="email" value="{{old('email')}}">
</div>

</div>
<div class="col-md-6"> 
<div class="form-group">
<label>Wiadomość</label>
<textarea  class="form-control form-control-user" name="wiadomosc">
{{old('wiadomosc')}}
</textarea>

</div>
<div class="form-group">
<input type="submit" class="btn btn-primary" value="Wyślij">
</div>
</div>

</div>

    </form>
</div>

    </div>
  
</main>
</div>
@endsection

