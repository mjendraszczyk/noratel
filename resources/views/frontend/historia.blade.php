@extends('layouts.frontend') 
@section('content')
<div class="container" style="clear:both;">

<h1 class="card-title" style="float:left;">Historia firmy</h1>
<ul class="timeline" style="clear:both;">
@foreach($historia as $key => $h)
@if(($key % 2) == 0)
<li class="timeline-inverted">
@else 
<li class="timeline">
@endif
<div class="timeline-badge primary">&nbsp;</div>
<div class="timeline-panel">
<div class="timeline-heading">
<h4 class="timeline-title">{{$h->tytul}}</h4>
</div>
<div class="timeline-body">
{!!$h->tresc!!}
</div>
</div>
</li>
<li>
@endforeach
</ul>

</div>
@endsection