@extends('layouts.frontend') 
@section('content')
<div class="container">
  <main>
  @foreach($items as $item)
    <div class="listing row aktualnosci">
      
      <div class="col-lg-8">
        <div class="card flat">
           
          <div class="card-body">
            <div class="feature_image" style="background-image:url({{asset('backend/uploads/newsy/'.$item->picture)}})">
            </div>
     <h1 class="card-title" style="padding:10px 0px;margin:15px 0;">
     <span class="first_letter">{{str_limit($item->tytul,1,'')}}</span>
     {{$item->tytul}}</h1>
            <p>
              {!!$item->tresc!!}
              <div class="clearfix"></div>
            </p>
          </div>
        </div>
      </div>
      
      <div class="col-lg-4">
      <div class="card flat listItems" style="margin-bottom:15px;">
          <div class="card-header">
       Aktualne oferty pracy
          </div> 
           <div class="card-body">
          @foreach($latestPraca as $praca)
  <h6><a href="{{route('praca_content',['id'=>$praca->id,'tytul'=>str_slug($praca->Nazwa)])}}">{{$praca->Nazwa}}</a></h6>
  <div class="row"> 
  <div class="col-md-5"> 
  <a href="{{route('praca_content',['id'=>$praca->id,'tytul'=>str_slug($praca->Nazwa)])}}">
  <img src="{{asset('backend/uploads/praca/'.$praca->picture)}}"/>
  </a>
</div>
<div class="col-md-7">
  {{str_limit(strip_tags(html_entity_decode($praca->Opis_krotki)),64)}}
</div>
</div>
          @endforeach
          </div>
          <div class="card-footer text-center">
            <a href="{{route('praca')}}">więcej</a>
          </div>
          
          </div>

        <div class="card flat listItems">
          <div class="card-header">
          Najnowsze aktualności
          </div> 
          <div class="card-body">
          @foreach($latestNews as $news)
  <h6><a href="{{route('aktualnosci_content',['id'=>$news->id,'tytul'=>str_slug($news->tytul)])}}">{{$news->tytul}}</a></h6>
  <div class="row"> 
  <div class="col-md-5"> 
  <a href="{{route('aktualnosci_content',['id'=>$news->id,'tytul'=>str_slug($news->tytul)])}}">
  <img src="{{asset('backend/uploads/newsy/'.$news->picture)}}"/>
  </a>
</div>
<div class="col-md-7">
  {!!str_limit(strip_tags($news->tresc),64)!!}
</div>
</div>
          @endforeach
          </div>
          <div class="card-footer text-center">
            <a href="{{route('aktualnosci')}}">więcej</a>
          </div>
          </div>
          </div>
    </div>
    @endforeach
  </main>
</div>
@endsection