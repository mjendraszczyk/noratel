@extends('layouts.frontend') 
@section('content')
<div class="container">
  <main>
  @foreach($items as $item)
    <h1 class="card-title" style="padding:10px 15px;margin:15px 0;">
    <span class="first_letter">{{str_limit($item->tytul,1,'')}}</span>
    {{$item->Nazwa}}
      
  </h1>
    <div class="listing row">
      
      <div class="col-lg-6" style="position:relative;z-index:3;">
        <div class="card flat">
          <div class="card-header">
          Szczegóły oferty
          </div> 
          <div class="card-body">
            <div class="feature_image" style="background-image:url({{asset('backend/uploads/praca/'.$item->picture)}})">
            </div>
            <p>
              {!!$item->Opis!!}
              </p>

</div>      
{{-- <div class="card-footer">
              <i class="fa fa-clock-o"></i> {{$item->created_at}}
            </div>      --}}  
            </div>
            </div>
            <div class="col-lg-6" data-aos="fade-right">
                        <form method="POST" action="{{route('praca_rekrutuj',['id'=>$item->id])}}" enctype="multipart/form-data" class="card flat">
                        <div class="card-header">
          Formularz zgłoszenia
          </div> 
@csrf
<div class="card-body">
 
@if ($errors->any())
    <div class="border-left-danger error-border">
        <ul class="card-body">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (!empty(Session::get('status')))
    <div class="border-left-success error-border">
        <ul class="card-body">
            {{Session::get('status')}}
        </ul>
    </div>
@endif

<div class="form-group">
    <label>Imię</label>
    <input  type="text" name="imie" class="form-control" value="{{old('imie')}}"/>
    </div>

<div class="form-group">
    <label>Nazwisko</label>
    <input  type="text" name="nazwisko" class="form-control" value="{{old('nazwisko')}}"/>
    </div>

    <div class="form-group">
    <label>E-mail</label>
    <input  type="email" name="email" class="form-control" value="{{old('email')}}" />
    </div>
    <div class="form-group">
    <label>Telefon</label>
    <input  type="text" name="telefon" class="form-control" value="{{old('telefon')}}"/>
    </div>
    <div class="form-group">
    <label>CV</label>
    <div class="clearfix"></div>
    <input  type="file" name="nazwaDokumentu" />
    </div>
    <div class="form-group">
    <label>Przetwarzanie danych osobowych</label>
    
    <div class="clearfix"></div>
    <label>
    <input  type="checkbox" name="zgoda_rodo" value="1" /> Wyrażam zgodę na przetwarzanie danych osobowych niezbędnych w celach rekrutacyjnych
    </label>
    </div>
        <input type="submit" class="btn btn-primary" style="width:100%;" value="Wyślij" />
     
                        </form>
              <div class="clearfix"></div>
</div>
 
            
          </div>
      </div>
      </div>
    @endforeach
  </main>
</div>
@endsection