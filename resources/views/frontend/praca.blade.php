@extends('layouts.frontend') 
@section('content')
<div class="container">
  <main>
    <h1 class="card-title">Oferty pracy</h1>
    <div class="listing row praca" style="width:100%;">
      @foreach($items as $item)
      <div class="col-lg-12" style="margin-bottom:25px;">
        <div class="card flat" data-aos="fade-up">
          <div class="row">
            <div class="col-lg-4 image" style="background-image:url({{asset('backend/uploads/praca/'.$item->picture)}})">
            </div>
            <div class="col-lg-8" style="    padding-left: 0;">
            {{--<div class="card-header">
            <i class="fa fa-clock-o"></i> Dodano: {{$item->created_at}}
            </div> --}}
            <div class="card-body">
<h2 class="m-0 font-weight-bold text-primary" style="padding: 50px 20px 50px 50px;"><a href="{{route('praca_content',['id'=>$item->id,'title'=>str_slug($item->Nazwa)])}}">{{$item->Nazwa}}</a></h2>
            

 
<div class="row" style="
    text-align: right;
">
 
            
<div class="col-md-12">
 <i class="fa fa-bar-chart" aria-hidden="true"></i> {{App\Http\Controllers\KategoriaController::getKategoriaNazwa($item->Kategoria)}}

<i class="fa fa-folder-o" style="
    margin: 0 0 0 10px;
"></i> {{App\Http\Controllers\backend\TypyUmowController::getTypyUmowNazwa($item->id_typumowy)}}
 
            </div>
            </div>
            
            <p>
              <!-- {{str_limit($item->Opis,180)}} -->
              <div class="clearfix"></div>
              <div class="row">
                <div class="col-md-6 text-right">
                  <span class="btn"><i class="fa fa-calendar-o"></i> {{App\Http\Controllers\backend\WymiarPracyController::getWymiarPracyNazwa($item->id_wymiarpracy)}}</span>
</div>
                <div class="col-md-6">
              <a class="btn btn-primary btn-user btn-block" href="{{route('praca_content',['id'=>$item->id,'title'=>str_slug($item->Nazwa)])}}"><img src="{{asset('frontend/img/icon-document.png')}}">Aplikuj</a>
              </div>
              </div>
            </p>
            </div>
            <div class="card-footer">
              <i class="fa fa-map-marker" aria-hidden="true"></i> {{config('custom.miejscowosc_firmy')}}, {{config('custom.kraj_firmy')}}
            </div>
            </div>
          </div>
        </div>
      </div>
      @endforeach
      {{ $items->links() }}
    </div>
  </main>
</div>
@endsection