@extends('layouts.frontend')
@section('content')
 <main>
 <div class="container" id="content" style="margin: auto;display: block;float: none;clear: both;">
<section class="oferty owl-carousel">
@foreach($prace as $praca)
<div class="row">
@if($praca->picture == '')
<div class="col-md-4 image" style="background-image:url('frontend/img/carousel1.png');">
@else 
<div class="col-md-4 image" style="background-image:url('backend/uploads/praca/{{$praca->picture}}');">
@endif
</div>
<div class="col-md-8">
<div class="oferta_content">
  <!--
<div class="owl-nav controllers"><button type="button" role="presentation" class="owl-prev btn controller prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" class="owl-next btn controller next"><span aria-label="Next">›</span></button></div>

   <div class="controllers">
    <span class="btn controller prev"><</span>
    <span class="btn controller next">></span>
  </div> -->
<h1>{{$praca->Nazwa}}</h1>
{{--<div class="oferta_data">
dodano {{$praca->created_at}}
</div>
<div class="oferta_place">
<i class="fas fa-map-marker-alt"></i> {{$praca->Miasto}}
</div>--}}
<div class="oferta_button">
<span class="oferta_wymiar">

</span>
<h4 style="color:#333;">Gwarantujemy</h4>
<div class="text-center">
@for($i=1;$i<13;$i++)
<img src="{{asset('frontend/img/ikony')}}/icon{{$i}}.png" style="width:64px;display:inline-block;"/>
@endfor
</div>
{{--
<span class="btn btn-secondary btn-lg">
@if(!empty(App\Http\Controllers\backend\WymiarPracyController::getWymiarPracyNazwa($praca->id_wymiarpracy))) 
{{App\Http\Controllers\backend\WymiarPracyController::getWymiarPracyNazwa($praca->id_wymiarpracy)}}
@else
Inny
@endif
</span>
--}}
 


<a href="{{route('praca_content',['id'=>$praca->id,'title'=>str_slug($praca->Nazwa)])}}" style="width:100%;" class="btn btn-primary btn-lg">
<img src="frontend/img/icon-document.png" style="display: inline-block !important;
    width: auto;" /> Aplikuj
</a>
</div>
</div>
</div>
</div>
@endforeach
</section>
 </div>

 <section class="ofirmie">
 <div class="container">
 <div class="row">
@foreach($ofirmie as $firma)
 <div class="col-md-6" data-aos="fade-up"   data-aos-duration="5000" style="padding:50px 25px;">
 <h1>O firmie</h1>
 <hr/>
 <div class="content">
 
{{$firma->krotki_opis}}
<a href="{{route('cms_content',['id'=>$firma->id,'title'=>str_slug($firma->tytul)])}}" class="btn btn-primary text-uppercase">więcej</a>
 </div>
 </div>
 <div class="col-md-6">
 @if($firma->picture != '')
 <img src="{{asset('backend/uploads/cms/'.$firma->picture)}}"  style="margin-top: -50px;"/>
 @else
<img src="{{asset('frontend/ofirmie.png')}}"  style="margin-top: -50px;"/>
 @endif
 </div>
 @endforeach
 </div>
 </div>
 </section>

 <section class="aktualnosci">
 <div class="container">
 <h1>aktualności</h1>

 <div class="aktualnosci">

 @foreach($Newsy as $key => $news)
 <div>
 <div class="news row" @if(($key%2) == 0) data-aos="fade-left" @else data-aos="fade-right" @endif>
 <div class="col-md-3 post_image" style="background-image:url({{asset('backend/uploads/newsy/'.$news->picture)}});">
 </div>
 <div class="col-md-9">
 <h3>{{$news->tytul}}</h3>
 <div>
 {{ str_limit(strip_tags(html_entity_decode($news->tresc)),320) }}
 </div>
 <a href="{{route('aktualnosci_content',['id'=>$news->id,'tytul'=>str_slug($news->tytul)])}}" class="btn btn-secondary text-uppercase">więcej</a>
 </div>
 </div>
</div>
 @endforeach

 

<div class="text-center">
<a href="{{route('aktualnosci')}}" class="btn btn-primary">
więcej
</a>
</div>
</div>
 </div>
 </section>
 </main>
 @endsection