@php echo '<?xml version="1.0" encoding="UTF-8"?>'; @endphp
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    @foreach($routes_array as $key => $route)
    <url>
        <loc>{{$route}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>1</priority>
        </url>
    @endforeach
    @foreach($getCms as $cms)
        <url>
        <loc>{{route('cms_content',['id'=>$cms->id,'title'=>str_slug($cms->tytul)])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
        </url>
    @endforeach

        @foreach($getPraca as $praca)
        <url>
        <loc>{{route('praca_content',['id'=>$praca->id,'title'=>str_slug($praca->Nazwa)])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
        </url>
    @endforeach

        @foreach($getNews as $news)
        <url>
        <loc>{{route('aktualnosci_content',['id'=>$news->id,'title'=>str_slug($news->tytul)])}}</loc>
        <lastmod>{{date('Y-m-d')}}</lastmod>
        <changefreq>daily</changefreq>
        <priority>0.5</priority>
        </url>
    @endforeach

</urlset>