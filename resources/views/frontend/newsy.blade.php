@extends('layouts.frontend') 
@section('content')
<div class="container">
  <main>
    <h1 class="card-title">Aktualności</h1>
    <div class="listing row">
      @foreach($items as $key => $item)
      <div class="col-lg-4" @if(($key%3) == 0) data-aos="fade-left" @elseif(($key % 3) == 2) data-aos="fade-right" @else data-aos="flip-left" @endif>
        <div class="card flat">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="{{route('aktualnosci_content',['id'=>$item->id,'tytul'=>str_slug($item->tytul)])}}">{{$item->tytul}}</a></h6>
          </div>
          <div class="card-body">
             <a href="{{route('aktualnosci_content',['id'=>$item->id,'tytul'=>str_slug($item->tytul)])}}">
               <div class="feature_image" style="background-image:url({{asset('backend/uploads/newsy/'.$item->picture)}})">
            </div>
            </a>
            <p style="padding:15px 0;">
              {!!str_limit(strip_tags(nl2br($item->tresc)),180)!!}
              <div class="clearfix"></div>
              <a href="{{route('aktualnosci_content',['id'=>$item->id,'tytul'=>str_slug($item->tytul)])}}" class="card-link">więcej</a>
            </p>
          </div>
          <div class="card-footer">
              <i class="fa fa-clock-o"></i> {{$item->created_at}}
            </div>   
        </div>
      </div>
      @endforeach
      {{ $items->links() }}
    </div>
  </main>
</div>
@endsection