<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{config('custom.title')}}</title>
  <meta name="description" content="{{config('custom.desc')}}" />
  <meta name="keywords" content="{{config('custom.keywords')}}" />
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
  <!-- Styles -->
  <link href="{{asset('frontend/css/style.css?v=')}}{{time()}}" rel='stylesheet' type='text/css' />
  <link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel='stylesheet' type='text/css' />
  <link rel='shortcut icon' type='image/x-icon' href="{{asset('favicon.ico')}}" />
  @if(config('custom.analytics') != '')
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id={{config('custom.analytics')}}"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', "{{config('custom.analytics')}}");
  </script>
  @endif
</head>

<body>
  <header class="header @if(Route::CurrentRouteName() == 'frontend')home @endif">
    <a href="http://noratel.com" target="_blank"
      style="background: #fff;color: #000;padding: 5px 10px;font-size: 0.9rem;vertical-align: middle;"><img
        src="{{asset('frontend/img/favicon.png')}}"
        style="padding: 0 10px 0 0;margin-top:-5px;vertical-align: middle;display: inline-block;">Strona koncernu
      Noratel</a>
    <div class="container">
      <div class="row">
        <nav class="navbar navbar-expand-lg navbar-light">
          <a href="{{route('frontend')}}" class="logo navbar-brand">
            <img src="{{asset('frontend/img/logo_big2.png')}}" />
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav top_menu">
              <img src="{{asset('frontend/img/logo-color.png')}}" class="d-block d-sm-block d-md-block d-lg-none"
                style="margin:50px auto 100px;display:block;" />

              @foreach($menus as $key => $menu)
              @if($menu->allowUrl == '1')
              <li class="nav-item @if($key == 0) active @endif">
                <a href="{{$menu->url}}" class="nav-link">{{$menu->label}}</a>
              </li>
              @endif
              @endforeach
            </ul>
          </div>
        </nav>
      </div>
      @if(Route::CurrentRouteName() == 'frontend')
      <div class="slogan">
        <h1>{{config('custom.slogan')}}</h1>
        {{--<h1>Transformatorów i dławników</h1>--}}
      </div>
      @endif
    </div>
  </header>