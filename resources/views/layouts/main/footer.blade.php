 <section class="footer-top">
<div class="footer_content">
<div class="container">
<div class="row panel" data-aos="fade-up">
<img src="{{asset('frontend/img/icon-worker.png')}}" /><h1>SPRAWDŹ AKTUALNE OFERTY PRACY</h1>
<a href="{{route('praca')}}" class="btn btn-primary text-uppercase">Oferty pracy</a>
</div>
</div>
</div>
 </section>
<footer class="footer-bottom">
 <div class="container">
 <div class="row">
 
 <div class="col-md-4">
 <img src="{{asset('frontend/img/logo-color.png')}}"/>
{{config('custom.nazwa_firmy')}}<br/>
{{config('custom.adres_firmy')}}<br/>
{{config('custom.kod_firmy')}} {{config('custom.miejscowosc_firmy')}}<br/>
{{config('custom.kraj_firmy')}}<br/>
NIP: {{config('custom.nip_firmy')}}<br/>
<br/>
{{config('custom.info_firmy')}}
 </div>
 <div class="col-md-4">
 <!-- <h6>Phone:</h6>
+48 91 425 06 74  -->
 <!-- <label>Sales</label>
+48 91 425 81 64 
 <label>Sales</label>
+48 91 425 06 75  -->
<h6>Sekretariat:</h6>
<i class="fa fa-phone"></i> {{config('custom.telefon_firmy')}}
<br/>
<i class="fa fa-envelope-o"></i> {{config('custom.email')}}
<h6 style="margin: 10px 0;">Kadry:</h6>
<i class="fa fa-phone"></i> {{config('custom.tel_kadry')}} 
<br/>
<i class="fa fa-envelope-o"></i> {{config('custom.mail_kadry')}} 
<h6 style="margin: 10px 0;">Sprzedaż:</h6>
<i class="fa fa-phone"></i>{{config('custom.tel_sprzedaz')}}  
<br/>
<i class="fa fa-envelope-o"></i> {{config('custom.mail_sprzedaz')}} 
 </div>
 <div class="col-md-4">
     <h6>Nawigacja:</h6>
 <ul>
       @foreach($menus as $key => $menu)
        <li>
        <a href="{{$menu->url}}">{{$menu->label}}</a>
      </li>
  @endforeach
</ul>
<div class="social">
@if(!empty(config('custom.facebook'))) 
    <a href="{{config('custom.facebook')}}" target="_blank"><i class="fa fa-facebook"></i></a>
    @endif
    @if(!empty(config('custom.instagram'))) 
    <a href="{{config('custom.instagram')}}" target="_blank"><i class="fa fa-instagram"></i></a>
    @endif
    @if(!empty(config('custom.google'))) 
    <a href="{{config('custom.google')}}" target="_blank"><i class="fa fa-google"></i></a>
    @endif
    @if(!empty(config('custom.linkedin'))) 
    <a href="{{config('custom.linkedin')}}" target="_blank"><i class="fa fa-linkedin"></i></a>
    @endif

    
</div>
developed by <a href="//mages.pl" target="_blank"><img src="{{asset('frontend/img/mages_brand.png')}}"  style="width: 75px;display: inline-block;"/></a>
 </div>
 </div>
 </div>
 <div class="phone_btn_flat new " id="phone28158441030305537" style="z-index: 999999;position: fixed;bottom: 0;/* background: #4e73df; */right: 0;display: block;padding: 5px 15px;border-radius: 50%;text-align: center;">
 <a href="mailto:agata-sw@noratel.pl">
 <img src="{{asset('frontend/img/hr_avatar.jpg')}}?v={{time()}}" class="tel_cbt" alt="#" style="
           display: inline-block;
    margin: -36px auto 0 auto;
    background: #4e73df;
    border-radius: 50%;"></a>
    
    <div class="help_cbt" style="
    display: inline-block;
    background: #fff;
    margin-left: -36px;
    position:relative;
        color: #4471b5;
    margin-top: 15px;
    padding-left: 40px;
">
<span id="close_dialog" class="close" style="cursor:pointer;"><i class="fa fa-close"></i></span>
Cześć!
<strong style="font-size: 0.9rem;">Jestem Agata</strong> i odpowiadam za rekrutację, <br>jeżeli chcesz do nas dołączyć lub masz pytania to zadzwoń:<br><strong style="color: #e1464a;">{{config('custom.callcenter')}}</strong>
</div></div>
 </footer>
        <script type="text/javascript" src="{{asset('frontend/js/app.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('frontend/js/aos.js')}}"></script>
        <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript">

          AOS.init();

        $('.owl-carousel').owlCarousel({
    loop:true,
    margin:12,
      autoplay:true,
    autoplayTimeout:3000,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
        </script>
                       <script>

    $(document).ready(function () {
   
   $("#close_dialog").click(function(e) {
    e.preventDefault();
       $(".phone_btn_flat").toggle();
   })

    $(".sortable").sortable({

        start: function (e, ui) {
            $(this).attr('data-previndex', ui.item.index());
        },
        update: function (e, ui) {
            var newIndex = ui.item.index();
            var oldIndex = $(this).attr('data-previndex');
            var element_id = ui.item.attr('id');


            $(this).children('li').each(function (dtx) {
                console.log("item" + dtx);
                $(this).children().find('.position_input').val(dtx);
                $(this).attr('data-pos', dtx);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                //w petli wykona sie ajax...
                $.ajax({
                    type: "post",
                    url: $(this).attr('data-url'), //data url z np elementu <UL>
                    data: { 'position': $(this).children().find('.position_input').val() },  // potrzebuje tu tak, numer pozycji 
                    success: function (data) {
                        if (dtx == 0) {
                            $('body').append("<div id='ajaxStatus'><i class='fa fa-thumbs-up'></i>" + data + "</div>'");
                            $("#ajaxStatus").delay(300).fadeOut(300);
                            setTimeout(function () {
                                $('#ajaxStatus').remove();
                            }, 3000);
                        }
                    }

                })

            });


            $(this).removeAttr('data-previndex');
        }
    });
    $(".sortable").disableSelection();
});
  </script>
    </body>
</html>