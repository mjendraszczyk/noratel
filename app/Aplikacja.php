<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aplikacja extends Model
{
    protected $table = 'aplikacja';

    protected $fillable = ['pracaID'];
}
