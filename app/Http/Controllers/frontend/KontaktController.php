<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;

use App\Http\Requests\frontend\KontaktRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\Kontakt;
use Session;

use App\Http\Controllers\Controller;

class KontaktController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.kontakt')->with('menus',$this->getMenu());
    }
 
    public function sendMail(KontaktRequest $request) { 

 
    $kontakt = $request->input('imie');
    $temat = $request->input('temat');
    $email = $request->input('kontakt');
    $wiadomosc = $request->input('wiadomosc');

    $data = array('temat'=>$temat, 'email'=>$email, 'wiadomosc'=>$wiadomosc);
     try { 
    Mail::send('mails.kontakt', $data, function($message) use ($email, $kontakt)
    {   
        $message->from(env('MAIL_USERNAME'), $kontakt);
        $message->to($email, $kontakt)->subject('Wiadomosc z strony Noratel');
    });
    } catch(Exception $e) {
        
    }


    Session::flash('status', 'Wysłano pomyślnie.');

 //       $data = $request->all();
//        Mail::to($request->input('email'),$request->input('imie'))->cc('michal.jendraszczyk@gmail.com')->send(new Kontakt($data));

        return redirect()->back();
    }
}
