<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\News;
use App\Praca;
use App\Cms;

class HomeController extends Controller
{
    public function index() 
    { 
    $Newsy = (new News())->where('stan','1')->where('home','1')->orderBy('id', 'desc')->limit(3)->get();
    $prace = (new Praca())->where('stan','1')->orderBy('id', 'desc')->limit(7)->get();

    $Ofirmie = (new Cms())->where('tytul','LIKE','%firmie%')->limit(1)->get();

     return view('frontend.index')->with('Newsy', $Newsy)->with('menus',$this->getMenu())->with('prace', $prace)->with('ofirmie',$Ofirmie);
    }
}
