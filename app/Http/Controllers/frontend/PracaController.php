<?php

namespace App\Http\Controllers\frontend;

use App\Praca;
use App\Aplikant;
use App\Aplikacja;

use Illuminate\Support\Facades\Mail;
use App\Mail\Kontakt;

use App\Http\Requests\frontend\PracaRequest;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
#use App\Http\Controllers\Controller\PracaController;

class PracaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prace = (new Praca())->where('stan','1')->paginate(10); //get();

        return view('frontend.praca')->with('items', $prace)->with('menus',$this->getMenu());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store($idPraca,PracaRequest $request)
    {

        $upload = new Controller();

        $upload->upload('nazwaDokumentu', 'pdf,doc,docx,odt', '/backend/uploads/dokumenty/', $request->input('nazwaDokumentu'), $request);

         
        $aplikant = new Aplikant();
        $aplikant->imie=$request->input('imie');
        $aplikant->nazwisko=$request->input('nazwisko');
        $aplikant->email=$request->input('email');
        $aplikant->telefon=$request->input('telefon');
        $aplikant->zgoda_rodo = ($request->input('zgoda_rodo') == '1') ? '1' : '0';
        $aplikant->save();

        $idAplikant = DB::getPdo()->lastInsertId();
        $aplikacja = new Aplikacja();
        $aplikacja->pracaID=$idPraca;
        $aplikacja->aplikantID=$idAplikant;
        $aplikacja->nazwaDokumentu = $upload->getFileName();
        $aplikacja->created_at = date('Y-m-d H:i:s', time());
        $aplikacja->updated_at = date('Y-m-d H:i:s', time());

        $aplikacja->save();

        $idAplikacja = DB::getPdo()->lastInsertId();

        //$praca = new App\Http\Controllers\Controller\backend\PracaController();        
        $data = array(
            'id' => $idAplikacja,
            'imie'=>$request->input('imie'),
            'nazwisko'=>$request->input('nazwisko'),
            'email'=>$request->input('email'),
            'telefon'=>$request->input('telefon'),
            'praca'=>$idPraca,
            'dokument'=> $upload->getFileName(),
        );

        $email = $data['email'];
        $kontakt = $request['imie'].' '.$request['nazwisko'];
        Mail::send('mails.nowa_aplikacja', $data, function($message) use ($email, $kontakt,$idAplikacja)
        {   
            $message->from(env('MAIL_USERNAME'), $kontakt);
            $message->to('kadry@noratel.pl', $kontakt)->cc(env('MAIL_USERNAME'))->subject('Dodano nowe zgłoszenie o pracę #'.$idAplikacja.' z strony Noratel');
        });

        Session::flash('status', 'Dodano pomyślnie.');
 //       $data = $request->all();
//        Mail::to($request->input('email'),$request->input('imie'))->cc('michal.jendraszczyk@gmail.com')->send(new Kontakt($data));

 
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Praca = (new Praca())->where('id',$id)->get();

        return view('frontend.praca_detail')->with('items',$Praca)->with('menus',$this->getMenu());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
