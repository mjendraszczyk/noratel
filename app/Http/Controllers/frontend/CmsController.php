<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cms;

class CmsController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items = (new Cms())->where('id',$id)->get();

        return view('frontend.cms')->with('items', $items)->with('menus',$this->getMenu());
    }
}
