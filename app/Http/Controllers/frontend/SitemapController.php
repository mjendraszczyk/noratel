<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\Praca;
use App\News;
use App\Cms;

class SitemapController extends Controller
{
    //----------------------------------------------------
    // WIDOK
    // SITEMAP
    //----------------------------------------------------

    // SITEMAP
     public function index() { 

        $routes_array = [route('frontend'),route('kontakt'),route('praca'),route('firma'),route('historia'),route('aktualnosci')];

        $getCms = (new Cms())->where('stan','1')->get();
        $getPraca = (new Praca())->where('stan','1')->get();
        $getNews = (new News())->where('stan','1')->get();
         
        return response()->view('frontend.sitemap.index',compact('getCms','getPraca','getNews','routes_array'))->header('Content-Type', 'text/xml');
     }
    
}
