<?php

namespace App\Http\Controllers\frontend;

use App\Historia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HistoriaController extends Controller
{
    public function index() { 
        $historia = (new Historia())->get();

        return view('frontend.historia',compact('historia'))->with('menus',$this->getMenu());
    }
}
