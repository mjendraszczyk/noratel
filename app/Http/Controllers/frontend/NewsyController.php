<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\News;
use App\Praca;
use App\Http\Controllers\Controller;

class NewsyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsy = (new News())->where('stan','1')->orderBy('id','desc')->paginate(9);

        return view('frontend.newsy')->with('items', $newsy)->with('menus',$this->getMenu());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $latestNews = (new News())->where('stan','1')->orderBy('id','desc')->limit(3)->get();
        $latestPraca = (new Praca())->where('stan','1')->orderBy('id','desc')->limit(3)->get();
        $News = (new News())->where('id',$id)->get();


       return view('frontend.news_detail')->with('items',$News)->with('latestNews',$latestNews)->with('latestPraca',$latestPraca)->with('menus',$this->getMenu());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
