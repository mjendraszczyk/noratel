<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Aplikacja;
use App\Menu;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $menus; 

    public function getMenu()  {
        $this->menus = (new Menu())->orderBy('position','asc')->get();

        return $this->menus;
    }
    
    public function upload($input, $extensions, $path, $tytul, $request)
    {
        $this->validate($request, [
            $input => 'file|mimes:'.$extensions.'|max:2048',
        ]);

        $image = $request->file($input);
        $this->FileName = md5(str_slug($tytul.time())).'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path($path);
        $imagePath = $destinationPath.'/'.$this->FileName;
        $image->move($destinationPath, $this->FileName);
    }

    public function getFileName()
    {
        return $this->FileName;
    }
    public static function getLastActivity() { 

        $aktywnosci = (new Aplikacja())->leftJoin('aplikant','aplikacja.aplikantID','=','aplikant.id')->orderBy('aplikacja.id','desc')->limit(3)->get();

        return $aktywnosci;
    }

}
