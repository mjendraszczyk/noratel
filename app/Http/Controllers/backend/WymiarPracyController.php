<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\WymiarPracy;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\backend\KategoriaRequest;

class WymiarPracyController extends Controller
{
       public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $typy = (new WymiarPracy())->get();

        return view('backend.wymiarpracy.index', compact('typy'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.WymiarPracy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(KategoriaRequest $request)
    {
        $typy = new WymiarPracy();

        $typy->insert([
            'Nazwa' => $request->get('Nazwa'),
        ]);

        $id = DB::getPdo()->lastInsertId();

        return redirect()->route('WymiarPracyEdit', ['id' => $id]);
    }

    public static function getWymiarPracyNazwa($id)
    {        
        $typy = WymiarPracy::where('id', $id)->get('Nazwa');

        if(count($typy)>0) {
        return @json_decode($typy, true)[0]['Nazwa'];
        } else {
            return false;
        }
        // return dd((json_encode(json_decode($kategoria, true))));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $typy = (new WymiarPracy())->where('id', $id)->get();

        
        return view('backend.wymiarpracy.edit', compact('typy', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(KategoriaRequest $request, $id)
    {
        $typy = (new WymiarPracy())->findOrFail($id);

        $typy->update([
            'Nazwa' => $request->get('Nazwa'),
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $typy = (new WymiarPracy())->findOrFail($id);
        $typy->delete();

        return redirect()->back();
    }
}
 