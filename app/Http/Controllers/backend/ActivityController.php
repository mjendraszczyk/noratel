<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Aplikacja;
use App\Aplikant;

class ActivityController extends Controller
{
    public function index(){ 

        $aktywnosci = (new Aplikacja())->leftJoin('aplikant','aplikacja.aplikantID','=','aplikant.id')->get();

        return view('backend.activity.index')->with('aktywnosci',$aktywnosci);
    }
    public static function other() {
        return "fs";
    }
}
