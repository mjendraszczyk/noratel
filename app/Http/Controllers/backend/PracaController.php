<?php

namespace App\Http\Controllers;

use App\Praca;
use Illuminate\Http\Request;
use App\Kategoria;
use App\TypyUmow;
use App\WymiarPracy;
use Illuminate\Support\Facades\DB;

use Session;
use App\Http\Requests\backend\PracaRequest;

class PracaController extends Controller
{
    public $getKategorie;
    public $getTypyUmow;
    public $getWymiarPracy;

    public function __construct()
    {
        $this->middleware('auth');

        $this->getKategorie = (new Kategoria())->get(['id', 'Nazwa']);
        
        $this->getTypyUmow = (new TypyUmow())->get(['id', 'Nazwa']);
        $this->getWymiarPracy = (new WymiarPracy())->get(['id', 'Nazwa']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prace = (new Praca())->get();

        return view('backend.praca.index', compact('prace'));
    }

    public static function getPracaNazwa($id) {
$praca = (new Praca())->where('id',$id)->get(['Nazwa']);

return @json_decode($praca, true)[0]['Nazwa'];
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.praca.create')->with('kategorie', $this->getKategorie)->with('TypyUmow',$this->getTypyUmow)->with('WymiarPracy',$this->getWymiarPracy);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PracaRequest $request)
    {
        $Praca = new Praca();

        $upload = new Controller();

        if($request->file('picture')) {
        $upload->upload('picture', 'jpeg,png,jpg', '/backend/uploads/praca/', $request->get('Nazwa'), $request);
        }

        $Praca->Nazwa = $request->get('Nazwa');
        $Praca->Kategoria = $request->get('Kategoria');
        $Praca->Opis_krotki = $request->get('Opis_krotki');
        $Praca->Opis = $request->get('Opis');
        if($request->file('picture')) {
        $Praca->picture = $upload->getFileName();
        } else {
            $Praca->picture = '404.jpg';
        }
                    $Praca->id_typumowy = $request->get('id_typumowy');
            $Praca->id_wymiarpracy = $request->get('id_wymiarpracy');
        $Praca->stan = !empty($request->get('Stan')) ? '1' : '0';

        $Praca->save();

        $id = DB::getPdo()->lastInsertId();

        Session::flash('status', 'Dodano pomyślnie.');
        return redirect()->route('PracaEdit', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $praca = (new Praca())->where('id', $id)->get();

        return view('backend.praca.edit', compact('praca', 'id'))->with('kategorie', $this->getKategorie)->with('TypyUmow',$this->getTypyUmow)->with('WymiarPracy',$this->getWymiarPracy);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(PracaRequest $request, $id)
    {
        // return dd($request);
        $praca = (new Praca())->findOrFail($id);

        if ($request->hasFile('picture')) {
            $upload = new Controller();
            $upload->upload('picture', 'jpeg,png,jpg', '/backend/uploads/praca/', $request->get('Nazwa'), $request);
            // echo 'ok'.$upload->getFileName();
            // exit();
        }

        $praca->update([
            'Nazwa' => $request->get('Nazwa'),
            'Opis_krotki' => $request->get('Opis_krotki'),
            'Opis' => $request->get('Opis'),
            'Kategoria' => $request->get('Kategoria'),
            'id_typumowy' => $request->get('id_typumowy'),
            'id_wymiarpracy' => $request->get('id_wymiarpracy'),
            'picture' => $request->hasFile('picture') ? $upload->getFileName() : $praca->picture,
            'stan' => !empty($request->get('Stan')) ? '1' : '0',
        ]);

        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $praca = (new Praca())->findOrFail($id);
        $praca->delete();

        return redirect()->back();
    }
}
