<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cms;
use Illuminate\Support\Facades\DB;
use Session;

use App\Http\Requests\backend\CmsRequest;

class CmsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cms = (new Cms())->get();

        return view('backend.cms.index', compact('cms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.cms.create');
    }

    public function store(CmsRequest $request)
    {
        $cms = new Cms();

        if ($request->hasFile('picture')) {
            $upload = new Controller();
            $upload->upload('picture', 'jpeg,png,jpg', '/backend/uploads/cms/', $request->get('tytul'), $request);
        }

        $cms->insert([
            'tytul' => $request->get('tytul'),
            'krotki_opis' => $request->get('krotki_opis'),
            'opis' => $request->get('opis'),
            'picture' => $request->hasFile('picture') ? $upload->getFileName() : $cms->picture,
            'stan' => !empty($request->get('Stan')) ? '1' : '0',
        ]);

        $id = DB::getPdo()->lastInsertId();
        Session::flash('status', 'Dodano pomyślnie.');
        //$request->session()->flash();
        return redirect()->route('CmsEdit', ['id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CmsRequest $request, $id)
    {
        // return dd($request);
        $cms = (new Cms())->findOrFail($id);

        if ($request->hasFile('picture')) {
            $upload = new Controller();
            $upload->upload('picture', 'jpeg,png,jpg', '/backend/uploads/cms/', $request->get('tytul'), $request);
        }

        //return dd($request);
        $cms->update([
          'tytul' => $request->get('tytul'),
            'krotki_opis' => $request->get('krotki_opis'),
            'opis' => $request->get('opis'),
            'picture' => $request->hasFile('picture') ? $upload->getFileName() : $cms->picture,
            'stan' => !empty($request->get('Stan')) ? '1' : '0',
        ]);
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->route('CmsEdit', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Cms = (new Cms())->where('id', $id)->get();
        //$getLink

        return view('backend.cms.edit', compact('Cms', 'id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cms = (new Cms())->findOrFail($id);
        $cms->delete();

        return redirect()->back();
    }
}
