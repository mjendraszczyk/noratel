<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aplikacja;
use App\Aplikant;
use File;
use App\Praca;
use App\Http\Requests\backend\AplikacjeRequest;
use Session;

use Illuminate\Support\Facades\DB;

class AplikacjaController extends Controller
{
    private $Aplikanci;
    public $FileName;

    public function __construct()
    {
        $this->middleware('auth');
        $this->Aplikanci = (new Aplikant())->get(['id', 'imie', 'nazwisko']);
        $this->Praca = (new Praca())->get(['id', 'Nazwa']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Aplikacja = (new Aplikacja())->get();

        return view('backend.aplikacje.index', compact('Aplikacja'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.aplikacje.create')->with('Aplikanci', $this->Aplikanci)->with('Prace', $this->Praca);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AplikacjeRequest $request)
    {
        $Aplikacja = new Aplikacja();

        $upload = new Controller();

        $upload->upload('nazwaDokumentu', 'pdf,doc,docx,odt', '/backend/uploads/dokumenty/', $request->get('nazwaDokumentu'), $request);

        $Aplikacja->insert([
            'pracaID' => $request->get('pracaID'),
            'aplikantID' => $request->get('aplikantID'),
            'nazwaDokumentu' => $upload->getFileName(),
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time()),
        ]);

        $id = DB::getPdo()->lastInsertId();

        Session::flash('status', 'Dodano pomyślnie.');

        return redirect()->route('AplikacjaEdit', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Aplikacja = (new Aplikacja())->where('id', $id)->get();

        return view('backend.aplikacje.edit', compact('Aplikacja', 'id'))->with('Aplikanci', $this->Aplikanci)->with('Prace', $this->Praca);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(AplikacjeRequest $request, $id)
    {
        $Aplikacja = (new Aplikacja())->findOrFail($id);

        $upload = new Controller();

        $upload->upload('nazwaDokumentu', 'pdf,doc,docx,odt', '/backend/uploads/dokumenty/', $request->get('nazwaDokumentu'), $request);

        $Aplikacja->update([
            'pracaID' => $request->get('pracaID'),
            'aplikantID' => $request->get('aplikantID'),
            'nazwaDokumentu' => $upload->getFileName(),
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time()),
        ]);

        $Aplikacja->save();
        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Aplikacja = (new Aplikacja())->findOrFail($id);

        foreach ($Aplikacja->get() as $aplikacja) {
            File::delete(public_path('/backend/uploads/dokumenty/').$aplikacja->nazwaDokumentu);
        }

        $Aplikacja->delete();

        return redirect()->back();
    }
}
