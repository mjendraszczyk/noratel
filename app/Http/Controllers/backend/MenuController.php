<?php

namespace App\Http\Controllers\backend;

use App\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use App\Http\Requests\backend\MenuRequest;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu = (new Menu())->get();

        return view('backend.menu.index', compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        $menu = new Menu();

        $menu->insert([
            'label' => $request->get('label'),
            'url' => $request->get('url'),
            'allowUrl' => !empty($request->get('allowUrl')) ? '1' : '0',
            'position' => ($menu->count()+1),
        ]);

        $id = DB::getPdo()->lastInsertId();
        Session::flash('status', 'Dodano pomyślnie.');

        return redirect()->route('MenuEdit', ['id' => $id]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = (new Menu())->where('id',$id)->get();

        return view('backend.menu.edit', compact('menu'))->with('id',$id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $request, $id)
    {
        $menu = (new Menu())->findOrFail($id);

        $menu->update([
            'label' => $request->get('label'),
            'url' => $request->get('url'),
            'allowUrl' => !empty($request->get('allowUrl')) ? '1' : '0',
            'position' => $request->get('position'),//($menu->count()+1),
        ]);

        $menu->save();
        Session::flash('status', 'Zaposano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = (new Menu())->findOrFail($id);
        $menu->delete();

        return redirect()->back();
    }
}
