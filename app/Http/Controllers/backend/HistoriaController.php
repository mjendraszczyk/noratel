<?php

namespace App\Http\Controllers\backend;

use App\Historia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use App\Http\Requests\backend\HistoriaRequest;

class HistoriaController extends Controller
{
    public function index()
    {
        $historia = (new Historia())->get();

        return view('backend.historia.index', compact('historia'));
    }
        public function create()
    {
        return view('backend.historia.create');
    }
        public function edit($id)
    {
        $historia = (new Historia())->where('id',$id)->get();

        return view('backend.historia.edit', compact('historia','id'));
    }
        public function store(Request $request)
    {
        $historia = new Historia();

        $historia->tytul = $request->get('tytul');
        $historia->tresc = $request->get('tresc');
        $historia->pozycja = $request->get('pozycja');
        $historia->save();

        return redirect()->route('HistoriaIndex');
    }
      public function update(Request $request, $id)
    {
        $historia = (new Historia())->findOrFail($id);

        $historia->update([
            'tytul' => $request->get('tytul'),
            'tresc' => $request->get('tresc'),
            'pozycja' => $request->get('pozycja'),
        ]);

        $historia->save();
        return redirect()->back();
        }
  public function destroy($id)
    {
        $historia = (new Historia())->findOrFail($id);
        $historia->delete();

        return redirect()->back();
    }
}
