<?php

namespace App\Http\Controllers\backend;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public $tablica_input;

    public function __construct()
    {
        $this->middleware('auth');

        $this->tablica_input = [
            ['Adres strony', 'site_url'],
            ['Główny E-mail', 'site_email'],
            ['Nazwa strony', 'site_name'],
            ['Opis strony', 'site_description'],
            ['Słowa kluczowe', 'site_keywords'],
            ['Facebook', 'site_fb'],
            ['Twitter', 'site_tw'],
            ['Google', 'site_google'],
            ['YouTube', 'site_yt'],
            ['Google Analytics UA Code', 'site_analytics'],
        ];
    }

    public function set_env(string $key, string $value, $env_path = null)
    {
        $value = preg_replace('/\s+/', '', $value); //replace special ch
    $key = strtoupper($key); //force upper for security
    $env = file_get_contents(isset($env_path) ? $env_path : base_path('.env')); //fet .env file
    $env = str_replace("$key=".env($key), "$key=".$value, $env); //replace value
        /** Save file eith new content */
        $env = file_put_contents(isset($env_path) ? $env_path : base_path('.env'), $env);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = (new Setting())->get();

        return view('backend.ustawienia.index', compact('settings'))->with('tablica_input', $this->tablica_input);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $settings = (new Setting())->get();

        // foreach ($settings as $key => $setting) {
        //     $sett = (new Setting())->where('name', $tablica_input[$key][1])->get();

        //     $sett->update([
        //         'value' => $request[$tablica_input[$key][1]],
        //     ]);

        //     $sett->save();
        // }

        foreach ($this->tablica_input as $key => $input) {
            $this->set_env(strtoupper($this->tablica_input[$key][1]), $request[$this->tablica_input[$key][1]]);
        }
        // putenv('CUSTOM_VARIABLE=hero');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
