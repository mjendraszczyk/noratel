<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Praca;
use App\Aplikacja;
use App\News;
use App\Cms;
use App\Aplikant;

class SearchController extends Controller
{
    public function index(Request $request) { 
      
        $query = $request->input('query');
         if(!empty($query))  {

            $result=[];
            $findPraca = (new Praca())->where('Nazwa','LIKE','%'.$query.'%')->OrWhere('Opis_krotki','LIKE','%'.$query.'%')->get();
            $findNews = (new News())->where('tytul','LIKE','%'.$query.'%')->OrWhere('tresc','LIKE','%'.$query.'%')->get();
            $findCms = (new Cms())->where('tytul','LIKE','%'.$query.'%')->OrWhere('krotki_opis','LIKE','%'.$query.'%')->get();
            $findAplikant = (new Aplikant())->where('imie','LIKE','%'.$query.'%')->OrWhere('nazwisko','LIKE','%'.$query.'%')->OrWhere('email','LIKE','%'.$query.'%')->get();

            $result[] = ["Praca","Newsy","Strony","Kandydaci","Dokumenty"];
            $result[] = $findPraca;
            $result[] = $findNews;
            $result[] = $findCms;
            $result[] = $findAplikant; 
        } else { 
            $result='0';
        }
                return view('backend.search.index')->with('result',$result)->with('query',$query);
    }

     
    public function show($query) { 
        
         if(!empty($query))  {

            $result=[];
            $findPraca = (new Praca())->where('Nazwa','LIKE','%'.$query.'%')->OrWhere('Opis_krotki','LIKE','%'.$query.'%')->get();
            $findNews = (new News())->where('tytul','LIKE','%'.$query.'%')->OrWhere('tresc','LIKE','%'.$query.'%')->get();
            $findCms = (new Cms())->where('tytul','LIKE','%'.$query.'%')->OrWhere('krotki_opis','LIKE','%'.$query.'%')->get();
            $findAplikant = (new Aplikant())->where('imie','LIKE','%'.$query.'%')->OrWhere('nazwisko','LIKE','%'.$query.'%')->OrWhere('email','LIKE','%'.$query.'%')->get();
            $findAplikacja = (new Aplikacja())->where('nazwaDokumentu','LIKE','%'.$query.'%')->get();

            $result[] = ["Praca","Newsy","Strony","Kandydaci","Dokumenty"];
            $result[] = $findPraca;
            $result[] = $findNews;
            $result[] = $findCms;
            $result[] = $findAplikant; 
            $result[] = $findAplikacja;
        } else { 
            $result='0';
        }
                return view('backend.search.index')->with('result',$result);
    }
}
