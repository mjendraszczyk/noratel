<?php

namespace App\Http\Controllers;

use App\Praca;
use App\Aplikacja;
use App\Aplikant;
use App\News;
use App\Kategoria;

class DashboardController extends Controller
{
    public $Kandydaci = '';
    public $Oferty = '';
    public $Dokumenty = '';
    public $Wizyty = '';

    public $newsy;
    public $prace;

    public $days = [];
    public $data = [];

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->Kandydaci = (new Aplikant())->count();
        $this->Oferty = (new Praca())->where('stan', '1')->count();
        $this->Dokumenty = (new Aplikacja())->count();
        $this->Wizyty = '';

        $this->newsy = (new News())->orderBy('id', 'desc')->limit(3)->get();
        $this->prace = (new Praca())->orderBy('id', 'desc')->limit(3)->get();

        $aplikacjeKategorie = (new Aplikacja())->join('praca', 'praca.id', '=', 'aplikacja.pracaID')->pluck('Kategoria');

        // Zakres kolorów
        $hex = [
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
        ];

        // Tablica kolorów
        $kolory = [];

        $kategorie = (new Kategoria())->get();
        foreach ($kategorie as $kategoria) {
            // Pojedyńczy kolor
            $kolor = '#';
            for ($z = 0; $z < 6; ++$z) {
                $kolor .= $hex[rand(0, count($hex) - 1)];
            }
            array_push($kolory, $kolor);
        }

        for ($i = 0; $i < date('t'); ++$i) {
            $liczDokumenty = (new Aplikacja())->where('created_at', 'LIKE',date('Y-m-'.($i + 1).' %'))->count();
            $this->days[$i] = ($i + 1);
            $this->data[$i] = $liczDokumenty;
        }

                $aktywnosci = (new Aplikacja())->leftJoin('aplikant','aplikacja.aplikantID','=','aplikant.id')->get();

        
    return view('backend.home')->with('Oferty', $this->Oferty)->with('Dokumenty', $this->Dokumenty)->with('Kandydaci', $this->Kandydaci)->with('Newsy', $this->newsy)->with('Prace', $this->prace)->with('aplikacjeKategorie', $aplikacjeKategorie)->with('days', $this->days)->with('data', $this->data)->with('liczDokumenty', $liczDokumenty)->with('kolory', $kolory)->with('aktywnosci',$aktywnosci);
    }
}
