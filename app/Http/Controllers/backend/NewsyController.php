<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Auth;
use Illuminate\Support\Facades\DB;
use Session;
use App\Http\Requests\backend\NewsyRequest;

class NewsyController extends Controller
{
    private $FileName;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsy = (new News())->get();

        return view('backend.newsy.index', compact('newsy'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.newsy.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newsy = new News();

        $upload = new Controller();
        if($request->file('picture')) {
        $upload->upload('picture', 'jpeg,png,jpg', '/backend/uploads/newsy/', $request->get('tytul'), $request);
        }
        $newsy->tytul = $request->get('tytul');
        $newsy->tresc = $request->get('tresc');
        if($request->file('picture')) {
        $newsy->picture = $upload->getFileName();
        } else{ 
            $newsy->picture = '404.jpg';
        }
        $newsy->user_id = Auth::id();
        $newsy->stan = !empty($request->get('Stan')) ? '1' : '0';
        $newsy->home = !empty($request->get('home')) ? '1' : '0';

        $newsy->save();

        $id = DB::getPdo()->lastInsertId();
Session::flash('status', 'Dodano pomyślnie.');
        return redirect()->route('NewsyEdit', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Newsy = (new News())->where('id', $id)->get();

        return view('backend.newsy.edit', compact('Newsy', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $newsy = (new News())->findOrFail($id);

        if ($request->hasFile('picture')) {
            $upload = new Controller();
            $upload->upload('picture', 'jpeg,png,jpg', '/backend/uploads/newsy/', $request->get('tytul'), $request);
        }
        $newsy->update([
            'tytul' => $request->get('tytul'),
            'tresc' => $request->get('tresc'),
            'user_id' => Auth::id(),
            'picture' => $request->hasFile('picture') ? $upload->getFileName() : $newsy->picture,
            'stan' => !empty($request->get('Stan')) ? '1' : '0',
            'home' => !empty($request->get('home')) ? '1' : '0',
        ]);

        $newsy->save();
        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $newsy = (new News())->findOrFail($id);
        $newsy->delete();

        return redirect()->back();
    }
}
