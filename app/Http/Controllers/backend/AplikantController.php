<?php

namespace App\Http\Controllers;

use App\Aplikant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Aplikacja;

use Session;
use App\Http\Requests\backend\AplikanciRequest;

class AplikantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Aplikant = (new Aplikant())->get();

        return view('backend.aplikanci.index', compact('Aplikant'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.aplikanci.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AplikanciRequest $request)
    {
        $Aplikant = new Aplikant();

        $Aplikant->insert([
            'imie' => $request->get('imie'),
            'nazwisko' => $request->get('nazwisko'),
            'telefon' => $request->get('telefon'),
            'email' => $request->get('email'),
            'zgoda_rodo' => !empty($request->get('zgoda_rodo')) ? '1' : '0',
        ]);

        $id = DB::getPdo()->lastInsertId();

        Session::flash('status', 'Dodano pomyślnie.');
        return redirect()->route('AplikanciEdit', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Aplikacje = (new Aplikacja())->where('aplikantID', $id)->get();
        $Aplikant = (new Aplikant())->where('id', $id)->get();

        return view('backend.aplikanci.edit', compact('Aplikant', 'id', 'Aplikacje'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(AplikanciRequest $request, $id)
    {
        $Aplikant = (new Aplikant())->findOrFail($id);

        $Aplikant->update([
            'imie' => $request->get('imie'),
            'nazwisko' => $request->get('nazwisko'),
            'telefon' => $request->get('telefon'),
            'email' => $request->get('email'),
            'zgoda_rodo' => !empty($request->get('zgoda_rodo')) ? '1' : '0',
        ]);
        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Aplikant = (new Aplikant())->findOrFail($id);
        $Aplikant->delete();

        return redirect()->back();
    }
    public static function getKandydatName($id) {
        try {
        $Aplikant = (new Aplikant())->where('id',$id)->get(['imie','nazwisko']);

        if(count($Aplikant)) {
            return @json_decode($Aplikant, true)[0]['imie'].' '.json_decode($Aplikant, true)[0]['nazwisko'];
        } else {
            return '<Nie podano>';
    }
    } catch(Exception $e) {
        return '';
    }
    }
}
