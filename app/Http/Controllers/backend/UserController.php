<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\backend\UserRequest;

use Session;
Session::flash('status', 'Dodano pomyślnie.');
class UserController extends Controller
{
    public $roles;

    public function __construct()
    {
        $this->middleware('auth');
        $this->roles = (new Role())->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = (new User())->get();

        return view('backend.user.index', compact('users'))->with('roles', $this->roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.user.create')->with('roles', $this->roles);
    }

    public function store(UserRequest $request)
    {
        $user = new User();

        $user->insert([
            'name' => $request->get('name'),
            'lastname' => $request->get('lastname'),
            'password' => bcrypt($request->get('password')),
            'email' => $request->get('email'),
            'lastname' => $request->get('lastname'),
            'id_role' => $request->get('id_role'),
        ]);

        $id = DB::getPdo()->lastInsertId();

        Session::flash('status', 'Dodano pomyślnie.');
        return redirect()->route('UserEdit', ['id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $User = (new User())->findOrFail($id);

        if((!empty($request->get('password'))) && ($request->get('repeat_password') == $request->get('password'))) { 
        $User->update([
          'name' => $request->get('name'),
            'lastname' => $request->get('lastname'),
            'password' => bcrypt($request->get('password')),
            'email' => $request->get('email'),
            'id_role' => $request->get('id_role'),
        ]);    
        } else {
        $User->update([
          'name' => $request->get('name'),
            'lastname' => $request->get('lastname'),
            'email' => $request->get('email'),
            'id_role' => $request->get('id_role'),
        ]);
        }
        
        Session::flash('status', 'Zapisano pomyślnie.');

        return redirect()->route('UserEdit', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = (new User())->where('id', $id)->get();

        return view('backend.user.edit', compact('user', 'id'))->with('roles', $this->roles);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = (new User())->findOrFail($id);
        $user->delete();

        return redirect()->back();
    }
}
