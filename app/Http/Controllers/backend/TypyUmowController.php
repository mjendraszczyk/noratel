<?php

namespace App\Http\Controllers\backend;


use App\Http\Controllers\Controller;

use App\TypyUmow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\backend\KategoriaRequest;
use Session;

class TypyUmowController extends Controller
{
   
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $typy = (new TypyUmow())->get();

        return view('backend.typyumow.index', compact('typy'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.typyumow.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(KategoriaRequest $request)
    {
        $typy = new TypyUmow();

        $typy->insert([
            'Nazwa' => $request->get('Nazwa'),
        ]);

        $id = DB::getPdo()->lastInsertId();

        Session::flash('status', 'Dodano pomyślnie.');
        return redirect()->route('TypyUmowEdit', ['id' => $id]);
    }

    public static function getTypyUmowNazwa($id)
    {
        $typy = TypyUmow::where('id', $id)->get('Nazwa');

        if(count($typy)>0) {
        return @json_decode($typy, true)[0]['Nazwa'];
        } else {
            return false;
        }
        // return dd((json_encode(json_decode($kategoria, true))));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $typy = (new TypyUmow())->where('id', $id)->get();

        return view('backend.typyumow.edit', compact('typy', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(KategoriaRequest $request, $id)
    {
        $typy = (new TypyUmow())->findOrFail($id);

        $typy->update([
            'Nazwa' => $request->get('Nazwa'),
        ]);

        Session::flash('status', 'Zapisano pomyślnie.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $typy = (new TypyUmow())->findOrFail($id);
        $typy->delete();

        return redirect()->back();
    }
}
