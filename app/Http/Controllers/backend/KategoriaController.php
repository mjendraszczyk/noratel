<?php

namespace App\Http\Controllers;

use App\Kategoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Session;
use App\Http\Requests\backend\KategoriaRequest;

class KategoriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategorie = (new Kategoria())->get();

        return view('backend.kategoria.index', compact('kategorie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.kategoria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(KategoriaRequest $request)
    {
        $Kategoria = new Kategoria();

        $Kategoria->insert([
            'Nazwa' => $request->get('Nazwa'),
        ]);

        $id = DB::getPdo()->lastInsertId();

        Session::flash('status', 'Dodano pomyślnie.');
        return redirect()->route('KategoriaEdit', ['id' => $id]);
    }

    public static function getKategoriaNazwa($id)
    {
        $kategoria = Kategoria::where('id', $id)->get('Nazwa');

        return @json_decode($kategoria, true)[0]['Nazwa'];
        // return dd((json_encode(json_decode($kategoria, true))));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Kategoria = (new Kategoria())->where('id', $id)->get();

        return view('backend.kategoria.edit', compact('Kategoria', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(KategoriaRequest $request, $id)
    {
        $Kategoria = (new Kategoria())->findOrFail($id);

        $Kategoria->update([
            'Nazwa' => $request->get('Nazwa'),
        ]);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Kategoria = (new Kategoria())->findOrFail($id);
        $Kategoria->delete();

        return redirect()->back();
    }
}
