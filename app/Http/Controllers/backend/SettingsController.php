<?php

namespace App\Http\Controllers\backend;

use App\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config;
use Artisan;
use Session;

class SettingsController extends Controller
{
    public $tablica_input;

    public function __construct()
    {
        $this->middleware('auth');

        $this->tablica_input = [
            ['Adres strony', 'site_url'],
            ['Główny E-mail', 'site_email'],
            ['Nazwa strony', 'site_name'],
            ['Opis strony', 'site_description'],
            ['Słowa kluczowe', 'site_keywords'],
            ['Facebook', 'site_fb'],
            ['Twitter', 'site_tw'],
            ['Google', 'site_google'],
            ['YouTube', 'site_yt'],
            ['Google Analytics UA Code', 'site_analytics'],
        ];
    }

    /**
     * zmienic envy na db.
     */
    public function set_env(string $key, string $value, $env_path = null)
    {
        $value = preg_replace('/\s+/', ' ', $value); //replace special ch
    $key = strtoupper($key); //force upper for security
    $env = file_get_contents(isset($env_path) ? $env_path : base_path('.env')); //fet .env file
    $env = str_replace("$key=".env($key), "$key='".$value."'", $env); //replace value
        /** Save file eith new content */
        $env = file_put_contents(isset($env_path) ? $env_path : base_path('.env'), $env);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = (new Setting())->get();

        return view('backend.ustawienia.index', compact('settings'))->with('tablica_input', $this->tablica_input);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $settings = (new Setting())->get();

        // foreach ($settings as $key => $setting) {
        //     $sett = (new Setting())->where('name', $tablica_input[$key][1])->get();

        //     $sett->update([
        //         'value' => $request[$tablica_input[$key][1]],
        //     ]);

        //     $sett->save();
        // }

        // foreach ($this->tablica_input as $key => $input) {
        //     $this->set_env(strtoupper($this->tablica_input[$key][1]), $request[$this->tablica_input[$key][1]]);
        // }
        //\Config::set('app.nazwa_firmy', 'Bablabla');
        //config(['custom.nazwa_firmy','poogpfkpogkpo']);
        Config::set([
        'custom.url' => $request->input('url'),
        'custom.email' => $request->input('email'),
        'custom.analytics' => $request->input('analytics'),
        'custom.title' => $request->input('title'),
        'custom.desc' => $request->input('desc'),
        'custom.keywords' => $request->input('keywords'),
        'custom.callcenter' => $request->input('callcenter'),
        
        'custom.info_firmy' => $request->input('info_firmy'),

        'custom.nazwa_firmy' => $request->input('nazwa_firmy'),
        'custom.adres_firmy' => $request->input('adres_firmy'),
        'custom.nip_firmy' => $request->input('nip_firmy'),
        'custom.telefon_firmy' => $request->input('telefon_firmy'),
        'custom.kod_firmy' => $request->input('kod_firmy'),
        'custom.wojewodztwo_firmy' => $request->input('wojewodztwo_firmy'),
        'custom.miejscowosc_firmy' => $request->input('miejscowosc_firmy'),
        'custom.kraj_firmy' => $request->input('kraj_firmy'),

        'custom.tel_kadry' => $request->input('tel_kadry'),
        'custom.mail_kadry' => $request->input('mail_kadry'),
        'custom.tel_sprzedaz' => $request->input('tel_sprzedaz'),
        'custom.mail_sprzedaz' => $request->input('mail_sprzedaz'),


        'custom.slogan' => $request->input('slogan'),

        'custom.google' => $request->input('google'),
        'custom.facebook' => $request->input('facebook'),
        'custom.instagram' => $request->input('instagram'),
        'custom.youtube' => $request->input('youtube'),
        'custom.linkedin' => $request->input('linkedin'),
        'custom.maintenance' => !empty($request->input('maintenance')) ? '1' : '0',

        ]);

        Config::set([
            'kontakty.email' => $request->input('email'),
            'kontakty.mail_kadry' => $request->input('mail_kadry'),
            'kontakty.mail_sprzedaz' => $request->input('mail_sprzedaz'),
        ]);
        $fp = fopen(base_path() .'/config/custom.php' , 'w');
        fwrite($fp, '<?php return ' . var_export(Config::get('custom'), true) . ';');
        fclose($fp);

        $fp = fopen(base_path() .'/config/kontakty.php' , 'w');
        fwrite($fp, '<?php return ' . var_export(Config::get('kontakty'), true) . ';');
        fclose($fp);

        Artisan::call('cache:clear');

        // return "fgdfg";
        // putenv('CUSTOM_VARIABLE=hero');
        Session::flash('status', 'Dodano pomyślnie.');

        return redirect()->route('SettingIndex');//->redirect()->route('SettingIndex');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
