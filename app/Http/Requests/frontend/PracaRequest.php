<?php

namespace App\Http\Requests\frontend;

use Illuminate\Foundation\Http\FormRequest;

class PracaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'imie' => 'required|min:2|max:255',
        'nazwisko' => 'required|min:2|max:255',
        'email' => 'required|email',
        'telefon' => 'required|numeric',
        'nazwaDokumentu' => 'required|file|mimes:pdf,doc,docx,odt',
        'zgoda_rodo' => 'required'
        ];
    }
}
