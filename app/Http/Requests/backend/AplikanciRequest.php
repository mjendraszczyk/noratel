<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class AplikanciRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'imie' => 'required|min:2|max:255',
        'nazwisko' => 'required|min:2|max:255',
        'email' => 'required|email',
        'aplikant_rodo' => 'required',
        ];
    }
}
