<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class PracaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Nazwa' => 'required|min:2|max:255',
            'Kategoria' => 'required',
            'Opis_krotki' => 'required|min:32|max:255',
            'Opis' => 'required|min:100',

        ];
    }
}
