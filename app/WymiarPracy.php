<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WymiarPracy extends Model
{
    protected $table = 'wymiarpracy';
    protected $fillable = ['Nazwa'];
}
