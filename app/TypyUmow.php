<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypyUmow extends Model
{
    protected $table = 'typyumow';
    protected $fillable = ['Nazwa'];
}
