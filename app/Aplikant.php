<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aplikant extends Model
{
    protected $table = 'aplikant';
    protected $fillable = ['imie','nazwisko','zgoda_rodo','telefon','email'];
}
