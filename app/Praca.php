<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Praca extends Model
{
    protected $table = 'praca';

    protected $fillable = ['Nazwa', 'Opis', 'Kategoria', 'picture', 'stan','Opis_krotki','id_typumowy','id_wymiarpracy'];
}
