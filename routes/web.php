<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Frontend
// use Url
// if(config('custom.maintenance') != '1') {
//     if (env('APP_ENV') === 'prod') {
//  URL::forceScheme('https');
// }


Route::get('/', 'frontend\HomeController@index')->name('frontend');

Route::get('/kontakt', 'frontend\KontaktController@index')->name('kontakt');
Route::post('/kontakt', 'frontend\KontaktController@sendMail')->name('sendMail');


Route::get('/praca', 'frontend\PracaController@index')->name('praca');

Route::get('/praca/{id}/{title}', 'frontend\PracaController@show')->name('praca_content');
Route::post('/praca/{id}/rekrutuj', 'frontend\PracaController@store')->name('praca_rekrutuj');

Route::get('/firma', 'frontend\FirmaController@index')->name('firma');
Route::get('/historia', 'frontend\HistoriaController@index')->name('historia');
Route::get('/aktualnosci', 'frontend\NewsyController@index')->name('aktualnosci');
Route::get('/aktualnosci/{id}/{title}', 'frontend\NewsyController@show')->name('aktualnosci_content');

Route::get('/cms/{id}/{title}', 'frontend\CmsController@show')->name('cms_content');
// } else {
// echo "Tryb konserwacji";
// }
// Sitemap Routing
Route::get('/sitemap.xml', 'frontend\SitemapController@index')->name('sitemap');

// Backend
Auth::routes();
// Poprawka przerzucenie usera do frontu
Route::get('/register',function() {
    return redirect()->route('frontend');
});
Route::get('/public/index.php', function() {
    return redirect()->route('backoffice');
});
Route::get('/backoffice', 'DashboardController@index')->name('backoffice');

Route::get('/backoffice/szukaj', 'backend\SearchController@index')->name('admin_search');

Route::get('/backoffice/aktywnosci', 'backend\ActivityController@index')->name('admin_aktywnosci');

Route::resource('/backoffice/praca', 'PracaController', [
    'names' => [
        'index' => 'PracaIndex',
        'create' => 'PracaCreate',
        'update' => 'PracaUpdate',
        'show' => 'PracaShow',
        'destroy' => 'PracaDestroy',
        'edit' => 'PracaEdit',
        'store' => 'PracaStore',
    ],
]);

Route::resource('/backoffice/kategoria', 'KategoriaController', [
    'names' => [
        'index' => 'KategoriaIndex',
        'create' => 'KategoriaCreate',
        'update' => 'KategoriaUpdate',
        'show' => 'KategoriaShow',
        'destroy' => 'KategoriaDestroy',
        'edit' => 'KategoriaEdit',
        'store' => 'KategoriaStore',
    ],
]);

Route::resource('/backoffice/typy-umow', 'backend\TypyUmowController', [
    'names' => [
        'index' => 'TypyUmowIndex',
        'create' => 'TypyUmowCreate',
        'update' => 'TypyUmowUpdate',
        'show' => 'TypyUmowShow',
        'destroy' => 'TypyUmowDestroy',
        'edit' => 'TypyUmowEdit',
        'store' => 'TypyUmowStore',
    ],
]);

Route::resource('/backoffice/wymiar-pracy', 'backend\WymiarPracyController', [
    'names' => [
        'index' => 'WymiarPracyIndex',
        'create' => 'WymiarPracyCreate',
        'update' => 'WymiarPracyUpdate',
        'show' => 'WymiarPracyShow',
        'destroy' => 'WymiarPracyDestroy',
        'edit' => 'WymiarPracyEdit',
        'store' => 'WymiarPracyStore',
    ],
]);

Route::resource('/backoffice/historia', 'backend\HistoriaController', [
    'names' => [
        'index' => 'HistoriaIndex',
        'create' => 'HistoriaCreate',
        'update' => 'HistoriaUpdate',
        'show' => 'HistoriaShow',
        'destroy' => 'HistoriaDestroy',
        'edit' => 'HistoriaEdit',
        'store' => 'HistoriaStore',
    ],
]);

Route::resource('/backoffice/pracownicy', 'UserController', [
    'names' => [
        'index' => 'UserIndex',
        'create' => 'UserCreate',
        'update' => 'UserUpdate',
        'show' => 'UserShow',
        'destroy' => 'UserDestroy',
        'edit' => 'UserEdit',
        'store' => 'UserStore',
    ],
]);

Route::resource('/backoffice/cms', 'CmsController', [
    'names' => [
        'index' => 'CmsIndex',
        'create' => 'CmsCreate',
        'update' => 'CmsUpdate',
        'show' => 'CmsShow',
        'destroy' => 'CmsDestroy',
        'edit' => 'CmsEdit',
        'store' => 'CmsStore',
    ],
]);

Route::resource('/backoffice/dokumenty', 'AplikacjaController', [
    'names' => [
        'index' => 'AplikacjaIndex',
        'create' => 'AplikacjaCreate',
        'update' => 'AplikacjaUpdate',
        'show' => 'AplikacjaShow',
        'destroy' => 'AplikacjaDestroy',
        'edit' => 'AplikacjaEdit',
        'store' => 'AplikacjaStore',
    ],
]);

Route::resource('/backoffice/role', 'RolaController', [
    'names' => [
        'index' => 'RolaIndex',
        'create' => 'RolaCreate',
        'update' => 'RolaUpdate',
        'show' => 'RolaShow',
        'destroy' => 'RolaDestroy',
        'edit' => 'RolaEdit',
        'store' => 'RolaStore',
    ],
]);

Route::resource('/backoffice/kandydaci', 'AplikantController', [
    'names' => [
        'index' => 'AplikanciIndex',
        'create' => 'AplikanciCreate',
        'update' => 'AplikanciUpdate',
        'show' => 'AplikanciShow',
        'destroy' => 'AplikanciDestroy',
        'edit' => 'AplikanciEdit',
        'store' => 'AplikanciStore',
    ],
]);

//Route::get('/backoffice/route-test', 'backend\SettingsController@index')->name('setting');

Route::resource('/backoffice/ustawienia', 'backend\SettingsController', [
    'names' => [
        'index' => 'SettingIndex',
        'create' => 'SettingCreate',
        'update' => 'SettingUpdate',
        'show' => 'SettingShow',
        'destroy' => 'SettingDestroy',
        'edit' => 'SettingEdit',
        'store' => 'SettingStore',
    ],
]);

Route::resource('/backoffice/menus', 'backend\MenuController', [
    'names' => [
        'index' => 'MenuIndex',
        'create' => 'MenuCreate',
        'update' => 'MenuUpdate',
        'show' => 'MenuShow',
        'destroy' => 'MenuDestroy',
        'edit' => 'MenuEdit',
        'store' => 'MenuStore',
    ],
]);

Route::resource('/backoffice/newsy', 'NewsyController', [
    'names' => [
        'index' => 'NewsyIndex',
        'create' => 'NewsyCreate',
        'update' => 'NewsyUpdate',
        'show' => 'NewsyShow',
        'destroy' => 'NewsyDestroy',
        'edit' => 'NewsyEdit',
        'store' => 'NewsyStore',
    ],
]);
