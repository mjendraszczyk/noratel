<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAplikantTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('aplikant', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('imie');
            $table->string('nazwisko');
            $table->string('telefon');
            $table->string('email');
            $table->enum('zgoda_rodo', ['1', '0']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('aplikant');
    }
}
