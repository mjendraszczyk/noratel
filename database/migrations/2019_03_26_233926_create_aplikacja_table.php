<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAplikacjaTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('aplikacja', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pracaID');
            $table->integer('aplikantID');
            $table->string('nazwaDokumentu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('aplikacja');
    }
}
