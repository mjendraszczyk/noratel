<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsyTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('newsy', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('tytul');
            $table->text('tresc');
            $table->string('picture');
            $table->string('stan');
            $table->integer('user_id');
            $table->integer('home')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('newsy');
    }
}
