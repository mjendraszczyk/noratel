<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePracaTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('praca', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('Nazwa');
            $table->text('Opis_krotki')->nullable();
            $table->text('Miasto')->nullable();
            $table->text('Opis')->nullable();
            $table->string('picture')->nullable();
            $table->integer('id_wymiarpracy')->nullable();
            $table->integer('id_typumowy')->nullable();
            $table->enum('stan', ['1', '0']);
            $table->integer('Kategoria');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('praca');
    }
}
