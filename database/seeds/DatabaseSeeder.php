<?php

use App\User;
use App\Praca;
use App\Kategoria;
use App\Role;
use App\Setting;
use App\News;
use App\Menu;
use App\WymiarPracy;
use App\TypyUmow;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        /**
         * Generate rows for categories.
         */
        $rola = new Role();
        $rola->Nazwa = 'Admin';
        $rola->save();

        $rola = new Role();
        $rola->Nazwa = 'Moderator';
        $rola->save();

        $user = new Kategoria();
        $user->Nazwa = 'Specjalista';
        $user->save();

        $user = new Kategoria();
        $user->Nazwa = 'Kierownik';
        $user->save();
        $user = new Kategoria();
        $user->Nazwa = 'Produkcja';
        $user->save();
        $user = new Kategoria();
        $user->Nazwa = 'Praktyki';
        $user->save();

        /**
         * Generate test rows for Praca.
         */
        $prace_img = [
        'c04cdb364b7dfe76c1c60c290288d60a.jpg',
        '1e689f71c3246554536069a017ad6b42.jpg',
        'ec26c82594cfd8a9a09065d6745c0a54.jpg',
        '72ee359c4e32c13191d0fc0e8cba8ce2.jpg',
        '1e71d3186e3cfde496481e7b3651891b.jpeg',
        '9f9d5dea93485335dfdfeb1538318f30.jpg',
        '21d3e89be753684e5d9a4fb831788ceb.jpg',
        ];

        $prace = ['Magazynier',
        'Specjalista ds logistyki',
        'Specjalista ds zarządzania jakoscia',
        'Monter',
        'Księgowa/y',
        'Specjalista ds sprzedazy',
        'Kierownik ds zarządzania łańcuchem dostaw', ];
        for ($v = 0; $v < count($prace); ++$v) {
            $praca = new Praca();
            $praca->Nazwa = $prace[$v];
            $praca->Opis = '';
            $praca->Stan = '1';
            $praca->Kategoria = '1';
            $praca->id_wymiarpracy = '1';
            $praca->id_typumowy = '1';
            $praca->picture = $prace_img[$v];
            $praca->save();
        }

        $user = new User();
        $user->name = 'Michał';
        $user->lastname = 'Jendraaszczyk';
        $user->email = 'example@example.com';
        $user->password = bcrypt('1');
        $user->email_verified_at = time();
        $user->id_role = '1';
        $user->save();

        $settings_tab = [
            ['site_url', 'site_url'],
            ['site_email', 'site_email'],
            ['site_name', 'site_name'],
            ['site_keywords', 'site_keywords'],
            ['site_description', 'site_description'],
            ['site_fb', 'site_fb'],
            ['site_tw', 'site_tw'],
            ['site_google', 'site_google'],
            ['site_yt', 'site_yt'],
            ['site_analytics', '1'],
        ];

        foreach ($settings_tab as $key => $s) {
            $settings = new Setting();
            $settings->name = $s[0];
            $settings->value = $s[1];
            $settings->save();
        }

        // $this->call(UsersTableSeeder::class);
        /*
         * Create news for db
         */

        //    protected $fillable = ['tytul', 'tresc', 'picture', 'stan'];

        $newsy_picutres = [
            '7c45130e1a091a373825066c9a55c449.jpeg',
            'aa4181c77fec9db47c8d047ac394e966.jpg',
            'fc60b16104fbaf1aef2c2f6596001edc.jpeg',
        ];
        for ($i = 0; $i < 7; ++$i) {
            $news = new News();

            $news->tytul = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
            $news->tresc = '
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ac velit at est pulvinar viverra. Donec pharetra posuere arcu, at imperdiet purus consequat a. In gravida neque id vehicula suscipit. Nulla eu dui ultrices, pretium mauris ac, volutpat sapien. Proin quis lacinia libero. Ut vel fermentum ligula. In ullamcorper mauris at lectus eleifend condimentum. Ut quis venenatis mi. In ultrices iaculis lacus id laoreet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus sit amet dictum velit. Donec accumsan quis magna molestie facilisis.
<br/>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ac velit at est pulvinar viverra. Donec pharetra posuere arcu, at imperdiet purus consequat a. In gravida neque id vehicula suscipit. Nulla eu dui ultrices, pretium mauris ac, volutpat sapien. Proin quis lacinia libero. Ut vel fermentum ligula. In ullamcorper mauris at lectus eleifend condimentum. Ut quis venenatis mi. In ultrices iaculis lacus id laoreet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus sit amet dictum velit. Donec accumsan quis magna molestie facilisis.

<br/>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ac velit at est pulvinar viverra. Donec pharetra posuere arcu, at imperdiet purus consequat a. In gravida neque id vehicula suscipit. Nulla eu dui ultrices, pretium mauris ac, volutpat sapien. Proin quis lacinia libero. Ut vel fermentum ligula. In ullamcorper mauris at lectus eleifend condimentum. Ut quis venenatis mi. In ultrices iaculis lacus id laoreet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus sit amet dictum velit. Donec accumsan quis magna molestie facilisis.
            ';
            $news->picture = $newsy_picutres[rand(0, count($newsy_picutres) - 1)];
            $news->stan = '1';
            $news->user_id = '1';

            $news->save();
        }


    // Dodanie menu 
        $menus = [ 
            [
                "label" => 'Strona główna',
                "url" => '/',
            ], 
            [
                "label" => 'O firmie',
                "url" => '/firma',
            ], 
            [
                "label" => 'Aktualnosci',
                "url" => '/aktualnosci',
            ], 
            [
                "label" => 'Praca',
                "url" => '/praca',
            ], 
            [
                "label" => 'Kontakt',
                "url" => '/kontakt',
            ], 
        ];

    foreach($menus as  $key => $menu) { 
        $menu = new Menu(); 
        $menu->label = $menus[$key]['label'];
        $menu->url = $menus[$key]['url'];
        $menu->position = $key;
        $menu->allowUrl = "1";

        $menu->save();
    }

    $wymiarPracy = new WymiarPracy();
$wymiarPracy->Nazwa = "Pełny etat";
            $wymiarPracy->save();

                $wymiarPracy = new WymiarPracy();
$wymiarPracy->Nazwa = "1/2 etatu";
            $wymiarPracy->save();

        $wymiarPracy = new WymiarPracy();
$wymiarPracy->Nazwa = "3/4 etatu";
            $wymiarPracy->save();

                $wymiarPracy = new WymiarPracy();
$wymiarPracy->Nazwa = "1/4 etatu";
            $wymiarPracy->save();
 

            $typyUmow = new TypyUmow();
$typyUmow->Nazwa = "Umowa o pracę";
            $typyUmow->save();

                $typyUmow = new TypyUmow();
$typyUmow->Nazwa = "Umowa zlecenie";
            $typyUmow->save();

                $typyUmow = new TypyUmow();
$typyUmow->Nazwa = "B2B";
            $typyUmow->save();

                $typyUmow = new TypyUmow();
$typyUmow->Nazwa = "Umowa o dzieło";
            $typyUmow->save();

    $typyUmow = new TypyUmow();
$typyUmow->Nazwa = "Inne";
            $typyUmow->save();
    }
}
